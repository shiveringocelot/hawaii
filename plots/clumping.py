import matplotlib.pyplot as plt
import random

d = 1

xs = []
ys = []
zs = []

x = random.uniform(-1, 1) ** 3
y = random.uniform(-1, 1) ** 3
z = random.uniform(-1, 1) ** 3

for i in range(0, 15000):
    x = random.uniform(0, d)
    y = random.uniform(0, d)
    z = random.uniform(0, d)

    # x = x ** 3
    # y = y ** 3
    # z = z ** 3

    l = (x*x + y*y + z*z) ** (1/2)
    # l = 1

    x = x/l
    y = y/l
    z = z/l

    xv = x**2
    yv = y**2
    zv = z**2

    lv = (xv*xv + yv*yv + zv*zv) ** (1/2)

    xv /= lv
    yv /= lv
    zv /= lv

    xs.append(x)
    ys.append(y)
    zs.append(z)

fig = plt.figure()
ax = fig.add_subplot(projection='3d')
  
# ax.plot(xs, ys, zs, linewidth=1)
ax.scatter(xs, ys, zs, s=0.1)
  
# function to show the plot
plt.show()