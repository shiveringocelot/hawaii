import math
import random

import matplotlib.pyplot as plt

xs = []
ys = []
zs = []

class Vec:
    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z

    def __neg__ (self):
        return Vec(-self.x, -self.y, -self.z)

    def __mul__ (self, s):
        return Vec(self.x * s, self.y * s, self.z * s)

    def __add__ (self, v):
        return Vec(self.x + v.x, self.y + v.y, self.z + v.z)

    def __pow__ (self, e):
        return Vec(self.x ** e, self.y ** e, self.z ** e)

    def normalize (self):
        l = (self.x**2 + self.y**2 + self.z ** 2) ** (1/2)
        self.x /= l
        self.y /= l
        self.z /= l
        return self

    def rand ():
        return Vec(random.uniform(-1,1), random.uniform(-1, 1), random.uniform(-1, 1))

# Constants
brown_strength = 0.5
bias_strength = 8
speed = 0.05

# Position
pos = Vec()

# Velocity
vel = Vec()

# Real velocity is the axis clumped base velocity

for i in range(0, 1800):
    # Apply a brownian force
    vel += Vec.rand() * brown_strength

    # Apply bound repulsion force
    vel += (-pos) ** 5

    # Keep the speed constant
    vel.normalize()

    # Apply directional bias to the movement of the particle
    bias_vec = vel ** 3
    movement = (vel + bias_vec * bias_strength).normalize()

    # Move the particle with resistance
    pos += movement * speed

    xs.append(pos.x)
    ys.append(pos.y)
    zs.append(pos.z)
    # zs.append(i)

fig = plt.figure()
ax = fig.add_subplot(projection='3d')
  
ax.plot(xs, ys, zs, linewidth=0.2)
# ax.scatter(xs, ys, zs, s=1)
  
# function to show the plot
plt.show()