#version 140
uniform mat4 cameraMatrix;
uniform float aspectRatio;

in vec2 position;

out vec3 vRay;
out vec2 vPos;

const float FOV = 0.5;

void main() {
    gl_Position = vec4(position, 0.0, 1.0);

    // No point in normalizing here because the interpolation will scale the ray in the fragment
    vec3 localRay = vec3(position.x * aspectRatio * FOV, position.y * FOV, 1.0);
    vec3 globalRay = (cameraMatrix * vec4(localRay, 1.0)).xyz;
    vRay = globalRay;
    vPos = position*0.5 + 0.5;
}