#version 140

uniform vec3 cameraPosition;
uniform vec3 lightDirection;

// The ray is not normalized, because it's just an interpolation from the vertex rays
in vec3 vRay;

out vec4 fColor;

const float EPSILON = 0.05;
const float DEPTH = 128.0;
const int MARCH_STEPS = 32;

//! UNIFORMS


vec4 orbit (vec3 p, vec3 orig) {
    vec3 v = abs(p - orig);
    return vec4(v, 0);
}


// Functions

float tetrahedronShape (vec3 p) {
    return (max(
        max(-p.x-p.y-p.z, p.x+p.y-p.z),
        max(-p.x+p.y+p.z, p.x-p.y+p.z)
    ) - 0.5) / sqrt(3);
}

float boxShape (vec3 p, float s) {
    vec3 a = abs(p.xyz) - s;
    return min(max(max(a.x, a.y), a.z), 0.0) + length(max(a, 0.0));
}

float coneShape(vec3 p, float h, float c)
{
  // c is the sin/cos of the angle, h is height
  // Alternatively pass q instead of (c,h),
  // which is the point at the base in 2D
  vec2 q = h*vec2(c, -1.0);
    
  vec2 w = vec2( length(p.xz), p.y );
  vec2 a = w - q*clamp( dot(w,q)/dot(q,q), 0.0, 1.0 );
  vec2 b = w - q*vec2( clamp( w.x/q.x, 0.0, 1.0 ), 1.0 );
  float k = sign( q.y );
  float d = min(dot( a, a ),dot(b, b));
  float s = max( k*(w.x*q.y-w.y*q.x),k*(w.y-q.y)  );
  return sqrt(d)*sign(s);
}

void mirrorFold(inout vec3 p, vec3 n) {
    p.xyz -= 2.0 * min(0.0, dot(p.xyz, n)) * n;
}
void sierpinskiFold(inout vec3 p) {
    p.xy -= min(p.x + p.y, 0.0);
    p.xz -= min(p.x + p.z, 0.0);
    p.yz -= min(p.y + p.z, 0.0);
}
void mengerFold(inout vec3 p) {
    /*float a = min(p.x - p.y, 0.0);
    p.x -= a;
    p.y += a;
    a = min(p.x - p.z, 0.0);
    p.x -= a;
    p.z += a;
    a = min(p.y - p.z, 0.0);
    p.y -= a;
    p.z += a;*/

    const vec2 k = vec2(-1, 1);
    p.xy += k * min(p.x - p.y, 0.0);
    p.xz += k * min(p.x - p.z, 0.0);
    p.yz += k * min(p.y - p.z, 0.0);
}
float sphereFold(inout vec3 p, float minR, float maxR) {
    float r2 = dot(p.xyz, p.xyz);
    float scale = max(maxR / max(minR, r2), 1.0);
    p *= scale;
    return scale;
}
void boxFold(inout vec3 p, vec3 r) {
    p.xyz = clamp(p.xyz, -r, r) * 2.0 - p.xyz;
}


//! COMPILED CODE



void main() {
  //fColor = vec4(1.0); return;

	vec3 ray = normalize(vRay);

	bool intercepted = false;
	float rayLength = 0.0;

    int i = 0;

    float dist;
    float minDist = 1e20;

	for (; i < MARCH_STEPS; i++) {
        vec3 point = cameraPosition + ray * rayLength;
        dist = sdf(point);

        minDist = min(minDist, dist);

        float eps = EPSILON;
        eps = EPSILON * rayLength * 0.2;

        rayLength += dist;

        // Sample is in the surface
        if (dist < eps) { break; }
        if (rayLength > DEPTH) { break; }
    }

    float val = 1.0 / (rayLength + 1.0);
    fColor = vec4(val, minDist, float(i) / MARCH_STEPS, 1.0);
}