#version 140

uniform vec3 cameraPosition;
uniform vec3 lightDirection;

// The ray is not normalized, because it's just an interpolation from the vertex rays
in vec3 vRay;

// Position in the precomputed texture
in vec2 vPos;

out vec4 fColor;

const float EPSILON = 0.01;
const float DEPTH = 128.0;
const int MARCH_STEPS = 64;

const vec3 skyTop = vec3(0.0, 0.25, 1.0);
const vec3 skyBottom = vec3(0.4, 0.7, 1.0);

const vec3 materialColor = vec3(1.0, 0.75, 0.5);
const vec3 materialShadow = vec3(0.8, 0.0, 0.0);

const vec3 sphereCenter = vec3(0.0, 0.0, 3.0);
const float sphereRadius = 1.0;

//! UNIFORMS

// This texture holds precomputed distance samples
uniform sampler2D precomputed;

// Colors
uniform vec3 uBgColor;
uniform vec3 uGlowColor;

uniform sampler3D uSurfaceColorSpace;


vec4 orbit (vec3 p, vec3 orig) {
    vec3 v = abs(p - orig);
    return vec4(v, 0);
}


// Functions

float tetrahedronShape (vec3 p) {
    return (max(
        max(-p.x-p.y-p.z, p.x+p.y-p.z),
        max(-p.x+p.y+p.z, p.x-p.y+p.z)
    ) - 0.5) / sqrt(3);
}

float boxShape (vec3 p, float s) {
    vec3 a = abs(p.xyz) - s;
    return min(max(max(a.x, a.y), a.z), 0.0) + length(max(a, 0.0));
}

float coneShape(vec3 p, float h, float c)
{
  // c is the sin/cos of the angle, h is height
  // Alternatively pass q instead of (c,h),
  // which is the point at the base in 2D
  vec2 q = h*vec2(c, -1.0);
    
  vec2 w = vec2( length(p.xz), p.y );
  vec2 a = w - q*clamp( dot(w,q)/dot(q,q), 0.0, 1.0 );
  vec2 b = w - q*vec2( clamp( w.x/q.x, 0.0, 1.0 ), 1.0 );
  float k = sign( q.y );
  float d = min(dot( a, a ),dot(b, b));
  float s = max( k*(w.x*q.y-w.y*q.x),k*(w.y-q.y)  );
  return sqrt(d)*sign(s);
}

void mirrorFold(inout vec3 p, vec3 n) {
    p.xyz -= 2.0 * min(0.0, dot(p.xyz, n)) * n;
}
void sierpinskiFold(inout vec3 p) {
    p.xy -= min(p.x + p.y, 0.0);
    p.xz -= min(p.x + p.z, 0.0);
    p.yz -= min(p.y + p.z, 0.0);
}
void mengerFold(inout vec3 p) {
    /*float a = min(p.x - p.y, 0.0);
    p.x -= a;
    p.y += a;
    a = min(p.x - p.z, 0.0);
    p.x -= a;
    p.z += a;
    a = min(p.y - p.z, 0.0);
    p.y -= a;
    p.z += a;*/

    const vec2 k = vec2(-1, 1);
    p.xy += k * min(p.x - p.y, 0.0);
    p.xz += k * min(p.x - p.z, 0.0);
    p.yz += k * min(p.y - p.z, 0.0);
}
float sphereFold(inout vec3 p, float minR, float maxR) {
    float r2 = dot(p.xyz, p.xyz);
    float scale = max(maxR / max(minR, r2), 1.0);
    p *= scale;
    return scale;
}
void boxFold(inout vec3 p, vec3 r) {
    p.xyz = clamp(p.xyz, -r, r) * 2.0 - p.xyz;
}


//! COMPILED CODE



// Using the tethaedron technique
// https://iquilezles.org/articles/normalsSDF/
vec3 surfaceNormal (vec3 p, float epsilon) {
    const vec2 k = vec2(1,-1);
    vec2 e = k*epsilon;

    return normalize( k.xyy * sdf( p + e.xyy ) + 
                      k.yyx * sdf( p + e.yyx ) + 
                      k.yxy * sdf( p + e.yxy ) + 
                      k.xxx * sdf( p + e.xxx ) );
}

vec3 surfaceShade (vec3 point, float epsilon) {
    vec3 orbit = color_field(point);
    orbit = 1 / (orbit*orbit + 1);

    vec3 color = texture(uSurfaceColorSpace, orbit).rgb;
    // color = orbit;

	vec3 normal = surfaceNormal(point, epsilon);
	float light = dot(normal, lightDirection);
    if (light > 0) {
        light = light*0.5 + 0.5;
    } else {
        light = light*0.2 + 0.5;
    }


	return color * light;
}

vec3 bgColor (vec3 ray, float dist) {
    float d = dist+1;
    float glow = 1 / (d);
    return mix(uBgColor, uGlowColor, clamp(glow, 0, 1));

    if (ray.y >= 0.0) {
    	return skyTop * ray.y + skyBottom * (1.0 - ray.y);
    } else {
        // Calculate the distance from x=z=0 for the intersections of xy and yz with y=-1
        float xd = ray.x / -ray.y;
        float zd = ray.z / -ray.y;
        float d = xd*xd+zd*zd;

        // Determine if (xd, zd) is in a dark or ligh square
        //bool isdark = (mod(xd, 1.0) > 0.5) ^^ (mod(zd, 1.0) > 0.5);
        //float v = isdark ? 0.4 : 0.6;
        float v = 0.6 - 1.0/(sqrt(d+10));
        return vec3(v);
    }
}

void main() {
    fColor.a = 1.0;
	vec3 ray = normalize(vRay);

    vec4 prePixel = texture(precomputed, vPos);

    // fColor = texture(precomputed, vPos); fColor.rg = fColor.gr; return;

	float rayLength = 0.0;
    rayLength = max(((1.0 - EPSILON*2) / prePixel.r) - 1.0, 0);

    int i = 0;

    float distExp = 0.0;

    float minDist = prePixel.g;

	for (; i < MARCH_STEPS; i++) {
        if (rayLength > DEPTH) { break; }

        vec3 point = cameraPosition + ray * rayLength;
        float dist = sdf(point);

        distExp = distExp * 0.2 + dist;

        float eps = EPSILON;
        eps = EPSILON * rayLength * 0.2;

        // Sample is in the surface
        if (dist < eps) {
        	vec3 color = surfaceShade(point, eps);

            float steps = i + prePixel.b * 32.0;
            float ao = clamp(10.0 / steps, 0, 1);
            color *= ao;

            float fog = clamp(1 / rayLength * 20, 0, 1);
            fColor.xyz = mix(bgColor(ray, minDist), color, fog);
            return;
        }
        rayLength += dist;
        minDist = min(minDist, dist);
    }

    fColor.xyz = bgColor(ray, minDist);

    // float glow = 1 / (16*minDist + 1.0);
    // fColor.xyz = vec3(1.0, 0.5, 0) * glow;

    //fColor.xyz = bgColor(ray);
}