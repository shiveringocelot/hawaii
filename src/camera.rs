
use std::cmp::Ordering;
use cgmath::{Matrix4, Matrix3, Rad, Transform, InnerSpace, Zero, One};
use crate::{Vec3, Scene, Params, Rand};

const PULL_DECAY: f32 = 5.0;

pub struct Camera {
    pub position: Vec3,
    pub aspect: f32,
    pub yaw: f32, // y-axis, x-movenemt
    pub pitch: f32, // x-axis, y-movement

    // Only captures rotation, not translation
    pub matrix: Matrix4<f32>,

    pub velocity: Vec3,

    pub gradient: f32,
    pub panic: bool,
}


#[derive(PartialEq,PartialOrd)]
struct NonNan(f32);
impl NonNan {
    fn new(val: f32) -> NonNan {
        if val.is_nan() {
            NonNan(f32::NEG_INFINITY)
        } else {
            NonNan(val)
        }
    }
}
impl Eq for NonNan {}
impl Ord for NonNan {
    fn cmp(&self, other: &NonNan) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

// Calculates the gradient over an axis
fn gradient_f (p: Vec3, e: Vec3, scene: &Scene, params: &Params) -> f32 {
    scene.sdf(p+e, params) - scene.sdf(p-e, params)
}

fn gradient_v (p: Vec3, m: Matrix3<f32>, scene: &Scene, params: &Params) -> Vec3 {
    let units = [Vec3::unit_x(), Vec3::unit_y(), Vec3::unit_z()];

    units.iter().map(|axis| {
        let r = m * axis;
        r * gradient_f(p, r, scene, params)
    }).reduce(|a, b| a+b).unwrap()
}

/// Generates all the vertices of  a tetrahedron with n subdivisions,
/// projected on the unit sphere
fn tetrahedron (n: u32) -> Vec<Vec3> {
    let vertices = [
        Vec3::new(0.0, 1.0, 1.0),
        Vec3::new(0.0, 1.0, -1.0),
        Vec3::new(-1.0, -1.0, 0.0),
        Vec3::new(1.0, -1.0, 0.0),
    ];

    let edges = [
        [0, 1], [2, 3],
        [0, 2], [0, 3],
        [1, 2], [1, 3usize],
    ];

    let faces = [
        [0, 1, 2], [0, 1, 3],
        [1, 2, 3], [1, 2, 3usize],
    ];

    let mut all = vec![];
    all.extend(vertices);

    // Add the edge subdivisions
    for [a_i, b_i] in edges {
        let a = vertices[a_i];
        let b = vertices[b_i];

        for i in 0..n {
            let a = a * (i+1) as f32;
            let b = b * (n-i) as f32;
            all.push((a+b) / (n+1) as f32);
        }
    }

    // Add the face subdivisions
    for [a_i, b_i, c_i] in faces {
        let a = vertices[a_i];
        let b = vertices[b_i];
        let c = vertices[c_i];

        for j in 0..n {
            for i in 0..(n-j) {
                let a = a * (i+1) as f32;
                let b = b * (n-j-i) as f32;
                let c = b * (j+1) as f32;
                all.push((a+b+c) / (n+2) as f32);
            }
        }
    }

    all
}

impl Camera {
    pub fn new () -> Self {
        Self {
            position: Vec3::zero(),
            yaw: 0.0,
            pitch: 0.0,
            aspect: 1.0,
            matrix: Matrix4::one(),
            velocity: Vec3::zero(),

            gradient: 0.0,
            panic: false,
        }
    }

    pub fn calculate_matrix (&mut self) {
        self.matrix =
            Matrix4::from_angle_y(Rad(self.yaw)) *
            Matrix4::from_angle_x(Rad(self.pitch));
    }

    pub fn translate (&mut self, dir: Vec3) {
        self.position += self.matrix.transform_vector(dir);
    }

    // Called when the camera is trapped
    fn escape_direction (&mut self, scene: &Scene, params: &Params) -> Vec3 {
        let mut radius = 1.0;
        let mut matrix = Matrix3::one();
        let normal = gradient_v(self.position, matrix, scene, params);

        let mut i = 0;
        loop {
            if i > 2 { return Vec3::zero(); }

            let vertices = tetrahedron(i);

            // Radius of the sphere necessary so that each vertex covers
            // pi m² of it's surface
            //let radius = (vertices.len() as f32).sqrt();

            // if radius=i+1, the area covered per vertex converges to 2pi m²
            // if radius=(i+1)², the area covered is: 8pi²x⁴/(x²+2), so it grows with x²
            let radius = ((i+1)*(i+1)) as f32;

            // Vertex of the (sphere projected) tetrahedron with origin in
            // the camera with the largest distance field
            let (direction, distance) = vertices.into_iter().map(|v| {
                let d = v.normalize();
                let p = self.position + d;
                let dist = scene.sdf(p, params);
                (d, dist)
            }).max_by_key(|p| NonNan(p.1)).unwrap();

            if distance > 1.0 {
                return direction;
            }

            i += 1;
        }
    }

    pub fn next_frame (&mut self, delta: f32, scene: &Scene, params: &Params, rand: &mut Rand) {
        let p = self.position;
        let distance = scene.sdf(p, params);

        // Sample the normals at 2/3 of the distance
        let r = distance * 0.66;
        let gradient = Vec3 {
            x: gradient_f(p, Vec3::unit_x() * r, scene, params),
            y: gradient_f(p, Vec3::unit_y() * r, scene, params),
            z: gradient_f(p, Vec3::unit_z() * r, scene, params),
        };

        let mag = gradient.magnitude();
        self.gradient = mag / r;

        if (distance > 0.8 || mag / r > 1.0) {
            let normal = gradient.normalize();
            let l = distance;

            // Can add push^5 if the push is too soft
            let push = (1.0-l);
            let push = (push.powi(3)).max(0.0);

            let pull = ((l*l)/PULL_DECAY+1.0).sqrt() - 1.0;

            let strength = push - pull;
            let vel = normal * strength;

            let amount = 0.1 * delta;

            // This is the first frame after a panic
            if self.panic {
                self.panic = false;
                self.velocity = Vec3::zero();
            }
            self.velocity = self.velocity * (1.0 - amount) + vel*amount;
        } else {
            self.panic = true;

            let dir = self.escape_direction(scene, params);
            self.velocity = dir;
            // TODO: Find the escape route
        }

        if self.velocity.magnitude() > 1.0 {
            self.velocity = self.velocity.normalize();
        }

        self.position += self.velocity * delta;
    }
}
