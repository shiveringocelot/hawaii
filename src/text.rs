
use glium_text_rusttype::{TextSystem, FontTexture, TextDisplay, draw};
use fontconfig::Fontconfig;

use glium::{Display, Frame, Surface, Program, VertexBuffer, IndexBuffer};

const HEIGHT: f32 = 18.0; // pixels
const WIDTH: f32 = 32.0; // characters

const QUAD_VSHADER: &'static str = r#"
	#version 140
	in vec2 position;
	uniform vec2 uSize;
	void main() { gl_Position = vec4(position * uSize + vec2(-1.0, 1.0), 0.0, 1.0); }
"#;

const QUAD_FSHADER: &'static str = r#"
	#version 140
	uniform vec4 uColor;
	out vec4 fColor;
	void main() { fColor = uColor; }
"#;

struct Quad {
	program: Program,
	vertices: VertexBuffer<Vertex>,
	indices: IndexBuffer<u16>,
}
#[derive(Copy, Clone)]
struct Vertex {
    position: [f32; 2],
}

glium::implement_vertex!(Vertex, position);

impl Quad {
	fn new (display: &Display) -> Self {
	    // building the vertex buffer, which contains all the vertices that we will draw
	    let vertices = {
	        VertexBuffer::new(display,
	            &[
	                Vertex { position: [0.0, -1.0] },
	                Vertex { position: [0.0,  0.0] },
	                Vertex { position: [1.0,  0.0] },
	                Vertex { position: [1.0, -1.0] },
	            ]
	        ).unwrap()
	    };

	    // building the index buffer
	    let indices = IndexBuffer::new(
	    	display,
	    	glium::index::PrimitiveType::TrianglesList,
			&[0u16, 1, 2, 0, 2, 3]
		).unwrap();

        let program = glium::program!(display,
            140 => {
                vertex: QUAD_VSHADER,
                fragment: QUAD_FSHADER
            },
        ).unwrap();

        Quad { vertices, indices, program }
	}

	fn draw (&self, frame: &mut Frame, size: [f32; 2], color: [f32; 4]) {
		let uniforms = glium::uniform!{
			uSize: size,
			uColor: color,
		};

		let params = {
	        use glium::BlendingFunction::Addition;
	        use glium::LinearBlendingFactor::*;

	        let blending_function = Addition {
	            source: SourceAlpha,
	            destination: OneMinusSourceAlpha
	        };

	        let blend = glium::Blend {
	            color: blending_function,
	            alpha: blending_function,
	            constant_value: (1.0, 1.0, 1.0, 1.0),
	        };

	        glium::draw_parameters::DrawParameters {
	            blend,
	            .. Default::default()
	        }
	    };
		frame.draw(&self.vertices, &self.indices, &self.program, &uniforms, &params).unwrap();
	}
}

pub struct Text {
	system: TextSystem,
	font: FontTexture,
	size: (f32, f32),
	lines: Vec<String>,
	quad: Quad,
}

impl Text {
	pub fn try_new (display: &Display) -> Result<Self, ()> {
	    let fc = Fontconfig::new().ok_or(())?;
	    // `Fontconfig::find()` returns `Option` (will rarely be `None` but still could be)
	    let font = fc.find("monospace", None).ok_or(())?;

	 	let system = TextSystem::new(display);

		// Creating a `FontTexture`, which a regular `Texture` which contains the font.
		// Note that loading the systems fonts is not covered by this library.
		let font = FontTexture::new(
		    display,
		    std::fs::File::open(&font.path).map_err(|_|())?,
		    32,
		    FontTexture::ascii_character_list()
		).map_err(|_|())?;

		Ok(Text { system, font, lines: vec![], size: (0.0, 0.0), quad: Quad::new(display) })
	}

	pub fn set_size (&mut self, w: f32, h: f32) {
		self.size = (w, h);
	}

	pub fn set_text (&mut self, lines: impl IntoIterator<Item=impl AsRef<str>>) {
		self.lines.clear();
		self.lines.extend(lines.into_iter().map(|x| x.as_ref().to_string()));
	}

	pub fn draw (&self, frame: &mut Frame) {
		let sx = 2.0 * HEIGHT / self.size.0;
		let sy = 2.0 * HEIGHT / self.size.1;

		let px = -1.0 + sx * 0.5;
		let py = 1.0 - sy;

		let mut matrix = [
			[sx , 0.0, 0.0, 0.0],
			[0.0, sy , 0.0, 0.0],
			[0.0, 0.0, 1.0, 0.0],
			[px , py , 0.0, 1.0]
		];

		let bg_color = [0.0, 0.0, 0.0, 0.5];
		let color = (1.0, 1.0, 1.0, 1.0);

		let quad_size = [sx * 0.5 * WIDTH, sy * (self.lines.len() + 1) as f32];
		self.quad.draw(frame, quad_size, bg_color);

		for line in &self.lines {
			let text = TextDisplay::new(&self.system, &self.font, &line);
			draw(&text, &self.system, frame, matrix, color);
			matrix[3][1] -= sy;
		}
	}
}