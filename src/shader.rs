
use std::fs::{read_to_string};
use glium::{Program, Display};
use crate::scene::Scene;

pub struct Shader {
    vertex_source: String,
    //fragment_source: String,
    //fragment_filename: String,
    pub pc_program: Program,
    pub px_program: Program,
}

impl Shader {
    pub fn create (display: &Display, scene: &Scene) -> Self {
        let v = read_to_string("shaders/vertex.glsl").unwrap();
        let (pc_f, px_f) = compile_fragment_source(scene).unwrap();

        let pc_prog = program!(display,
            140 => {
                vertex: &v,
                fragment: &pc_f
            },
        ).unwrap();

        let px_prog = program!(display,
            140 => {
                vertex: &v,
                fragment: &px_f
            },
        ).unwrap();

        Self {
            vertex_source: v,
            //fragment_source: f,
            pc_program: pc_prog,
            px_program: px_prog,
        }
    }

    pub fn reload (&mut self, display: &Display, scene: &Scene) {
        let (pc_src, px_src) = match compile_fragment_source(scene) {
            Ok(tuple) => tuple,
            Err(err) => {
                println!("{}", err);
                return;
            }
        };

        let pc_prog = match program!(display,
            140 => {
                vertex: &self.vertex_source,
                fragment: &pc_src
            },
        ) {
            Ok(p) => p,
            Err(err) => {
                println!("Error compiling glsls program:\n{:?}", err);
                return;
            }
        };

        let px_prog = match program!(display,
            140 => {
                vertex: &self.vertex_source,
                fragment: &px_src
            },
        ) {
            Ok(p) => p,
            Err(err) => {
                println!("Error compiling glsls program:\n{:?}", err);
                return;
            }
        };

        //self.fragment_source = pc_src;
        self.pc_program = pc_prog;
        self.px_program = px_prog;
    }
}

fn compile_fragment_source (scene: &Scene) -> Result<(String, String), String> {
    let pc_template = read_to_string("shaders/precompute.fragment.glsl").unwrap();
    let px_template = read_to_string("shaders/fragment.glsl").unwrap();

    let uniforms = scene.uniforms_line();
    let code = scene.shader_code();

    let pc_code = pc_template
        .replace("//! COMPILED CODE", &code)
        .replace("//! UNIFORMS", &uniforms);

    let px_code = px_template
        .replace("//! COMPILED CODE", &code)
        .replace("//! UNIFORMS", &uniforms);

    // Try to write fragment, for debugging purposes
    if true || cfg!(target="debug") {
        if let Err(err) = std::fs::write(".fragment.glsl", &px_code) {
            println!("Cannot write debug shader: {:?}", err);
        }
    }

    Ok((pc_code, px_code))
}
