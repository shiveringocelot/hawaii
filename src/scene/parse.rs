
// TODO: Move all parsing to crate::parse

use super::*;
use crate::params::ParamDef;

pub struct ParseOutput {
	pub items: Vec<Item>,
	pub params: Vec<ParamDef>,
}

enum ParsedLine {
	Item(Item),
	Empty,
	ParamDef {
		name: String,
		low: Option<f32>,
		high: Option<f32>,
		force: Option<f32>,
	},
	Loop(usize),
	End,
}

trait ToParseError<T> {
	fn parse_error(self, s: impl ToString) -> Result<T, Error>;
}

impl <T, E> ToParseError<T> for Result<T, E> {
	fn parse_error(self, s: impl ToString) -> Result<T, Error> {
		self.map_err(|_|parse_error(s))
	}
}

impl <T> ToParseError<T> for Option<T> {
	fn parse_error(self, s: impl ToString) -> Result<T, Error> {
		self.ok_or(parse_error(s))
	}
}

fn parse_int (s: &str) -> Result<usize, Error> { s.parse().parse_error(format!("Not a number: {}", s)) }
fn parse_float (s: &str) -> Result<f32, Error> { s.parse().parse_error(format!("Not an integer: {}", s)) }

fn parse_line (line: &str) -> Result<ParsedLine, Error> {
	let line = line.replace("\t", "");
	let words: Vec<_> = line
		.split(" ")
		.filter(|s| !s.is_empty())
		.collect();

	if words.is_empty() || words[0].starts_with("#") { return Ok(ParsedLine::Empty); }

	Ok(ParsedLine::Item(match words[0] {
		"param" => {
			let name = words[1].to_string();
			let (low, high, force) = match words.len() {
				2 => (None, None, None),
				3 => (None, Some(parse_float(words[2])?), None),
				4 => (Some(parse_float(words[2])?), Some(parse_float(words[3])?), None),
				5 => (Some(parse_float(words[2])?), Some(parse_float(words[3])?), Some(parse_float(words[4])?)),
				_ => return Err(parse_error("Too many arguments")),
			};
			return Ok(ParsedLine::ParamDef {
				name, low, high, force
			});
		},

		"sphere" => {
			Sphere(parse_sym(words[1])?).to_item()
		},
		"cube" => {
			Cube(parse_sym(words[1])?).to_item()
		},


		"scale" => {
			Scale(parse_sym(words[1])?).to_item()
		},
		"translate" => {
			Translate{
				x: parse_sym(words[1])?,
				y: parse_sym(words[2])?,
				z: parse_sym(words[3])?,
			}.to_item()
		},
		"rotate" => {
			use Sym::Val;
			let (x, y, z, angle) = match words[1] {
				"x" => (Val(1.0), Val(0.0), Val(0.0), words[2]),
				"y" => (Val(0.0), Val(1.0), Val(0.0), words[2]),
				"z" => (Val(0.0), Val(0.0), Val(1.0), words[2]),
				_ => (
						parse_sym(words[1])?,
						parse_sym(words[2])?,
						parse_sym(words[3])?,
						words[4]
					),
			};

			Rotate {
				x, y, z, angle: parse_sym(angle)?
			}.to_item()
		},
		"mirror" => {
			match words[1] {
				"xyz" => MirrorXYZ.to_item(),
				"x" => MirrorAxis(Axis::X).to_item(),
				"y" => MirrorAxis(Axis::Y).to_item(),
				"z" => MirrorAxis(Axis::Z).to_item(),
				_ => {
					MirrorPlane{
						x: parse_sym(words[1])?,
						y: parse_sym(words[2])?,
						z: parse_sym(words[3])?,
					}.to_item()
				}
			}
		},

		"orbit" => {
			Item::Orbit {
				x: parse_sym(words[1])?,
				y: parse_sym(words[2])?,
				z: parse_sym(words[3])?,
			}
		}


		"loop" => {
			return Ok(ParsedLine::Loop(parse_int(words[1])?));
		},
		"end" => {
			return Ok(ParsedLine::End);
		},
		cmd => {
			return Err(parse_error(format!("Unknown command: {}", cmd)))
		},
	}))
}

enum ScopeType { Top, Loop(usize) }
struct Scope {
	tp: ScopeType,
	items: Vec<Item>,
}

pub fn parse_lines (lines: Vec<String>) -> Result<ParseOutput, Error> {

	let mut params = vec![];
	let mut stack = vec![];
	let mut scope = Scope {
		tp: ScopeType::Top,
		items: vec![],
	};

	for line in lines {
		match parse::parse_line(line.as_ref())? {
			ParsedLine::Item(item) => scope.items.push(item),
			ParsedLine::Loop(count) => {
				let new_scope = Scope {
					tp: ScopeType::Loop(count),
					items: vec![],
				};
				stack.push(std::mem::replace(&mut scope, new_scope));
			},
			ParsedLine::End => {
				let old = std::mem::replace(&mut scope, stack.pop().unwrap());
				match old.tp {
					ScopeType::Loop(count) => {
						scope.items.push(Item::Loop { count, items: old.items })
					},
					ScopeType::Top => {
						return Err(parse_error("Unmatched end line"))
					}
				}
			},
			ParsedLine::Empty => {},
			ParsedLine::ParamDef { name, low, high, force } => {
				let sum = low.unwrap_or(0.0);
				let mul = high.unwrap_or(1.0) - sum;
				params.push(ParamDef {
					name: Some(name),
					index: params.len(),
					sum, mul,
					force: force.unwrap_or(1.0),
					repeat: false,
				});
			},
			_ => unimplemented!(),
		}
	}

	if !stack.is_empty() {
		return Err(parse_error("Unclosed scope"));
	}

	Ok(ParseOutput { items: scope.items, params })
}
