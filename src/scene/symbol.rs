
use super::VarSrc;

type BSym = Box<Sym>;

#[derive(Clone, Debug, PartialEq)]
pub enum Sym {
	Val(f32),
	Var(String),
	Neg(BSym),
	Inv(BSym),
	Sum(Vec<Sym>),
	Mul(Vec<Sym>),
	Access(BSym, Field),
	Call(String, Vec<Sym>),
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Field { X, Y, Z, W, XY, XZ, YZ, XYZ }

use Sym::*;

impl Sym {
    pub fn eval (&self, vars: &VarSrc<'_>) -> f32 {
        match self {
            Val(f) => *f,
            Var(name) => vars.get(&name).eval(vars),
            Neg(v) => -(v.eval(vars)),
            Inv(v) => 1.0/v.eval(vars),
            Sum(terms) => terms.iter().map(|t| t.eval(vars)).sum(),
            Mul(terms) => terms.iter().map(|t| t.eval(vars)).fold(1.0, |a, b| a*b),
            sym => unimplemented!("Symbol Eval: {}", sym),
        }
    }

	pub fn negate (self) -> Sym {
		match self {
			Val(f) => Val(-f),
			Neg(b) => *b,
			sym => Neg(Box::new(sym)),
		}
	}

	pub fn negate_mut (&mut self) {
		*self = std::mem::replace(self, Val(0.0)).negate();
	}

    pub fn add (a: Sym, b: Sym) -> Sym {
        let mut c = vec![];
        let mut f = |a: Sym| match a {
            Sym::Sum(a) => c.extend(a.into_iter()),
            a => c.push(a),
        };
        f(a); f(b);
        Sym::Sum(c)
    }

    pub fn mul (a: Sym, b: Sym) -> Sym {
        let mut c = vec![];
        let mut f = |a: Sym| match a {
            Sym::Mul(a) => c.extend(a.into_iter()),
            a => c.push(a),
        };
        f(a); f(b);
        Sym::Mul(c)
    }

    pub fn optimize_mut (&mut self) {
        match self {
            Sym::Call(s, args) => {
                args.iter_mut().for_each(Sym::optimize_mut);
            },
            Sym::Mul(terms) => {
                terms.iter_mut().for_each(Sym::optimize_mut);
                if terms.iter().any(|x| *x == Sym::Val(0.0)) {
                    *self = Sym::Val(0.0);
                } else {
                    *terms = terms.drain(..).filter(|x| *x != Sym::Val(1.0)).collect();
                    if terms.is_empty() {
                        *self = Sym::Val(1.0);
                    }
                }
            },
            Sym::Sum(terms) => {
                terms.iter_mut().for_each(Sym::optimize_mut);
                *terms = terms.drain(..).filter(|x| *x != Sym::Val(0.0)).collect();
                if terms.is_empty() {
                    *self = Sym::Val(0.0);
                }
            },
            _ => {}
        }
    }
}

impl Default for Sym {
    fn default () -> Self { Sym::Val(0.0) }
}

impl std::fmt::Display for Sym {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		use std::fmt::Display;
        fn join (f: &mut std::fmt::Formatter<'_>, syms: &[Sym], sep: &'static str) -> std::fmt::Result {
            let mut iter = syms.iter();
            if let Some(arg) = iter.next() {
                arg.fmt(f)?;
            }
            for arg in iter {
                write!(f, "{}{}", sep, arg)?;
            }
            Ok(())
        }
    	match self {
    		Val(v) => v.fmt(f),
            Var(s) => write!(f, "{}", s),
    		Neg(i) => write!(f, "-{}", i),
    		Inv(i) => write!(f, "(1.0/{})", i),
            Sum(xs) => join(f, &xs, "+"),
            Mul(xs) => join(f, &xs, "*"),
            Access(val, field) => write!(f, "{}.{}", val, field),
    		Call(name, syms) => {
    			write!(f, "{}(", name)?;
    			join(f, &syms, ",")?;
    			write!(f, ")")
    		}
    	}
    }
}

impl std::fmt::Display for Field {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		use std::fmt::Display;
    	use Field::*;
    	let s = match self {
    		X => "x", Y => "y", Z => "z", W => "w",
    		XY => "xy", XZ => "xz", YZ => "yz",
    		XYZ => "xyz",
    	};
		s.fmt(f)
    }
}

mod parse {
    use super::*;
    use crate::scene::{Error, parse_error};

    type Precedence = u8;

    #[derive(Debug)]
    enum Token { Open, Close, Comma, Plus, Minus, Mul, Div, Str(String) }
    impl Token {
        fn precedence (&self) -> Option<Precedence> {
            use Token::*;
            match self {
                Open | Close | Comma | Str(_) => None,
                Plus | Minus => Some(1),
                Mul | Div => Some(2),
            }
        }
    }

    fn get_tokens (input: &str) -> Vec<Token> {
        let mut s = String::new();
        let mut tokens = vec![];

        for c in input.chars() {
            let tk = match c {
                '(' => Some(Token::Open),
                ')' => Some(Token::Close),
                ',' => Some(Token::Comma),
                '+' => Some(Token::Plus),
                '-' => Some(Token::Minus),
                '*' => Some(Token::Mul),
                '/' => Some(Token::Div),
                c => None
            };

            if let Some(tk) = tk {
                if !s.is_empty() {
                    tokens.push(Token::Str(std::mem::replace(&mut s, String::new())));
                }
                tokens.push(tk);
            } else {
                s.push(c);
            }
        }

        if !s.is_empty() {
            tokens.push(Token::Str(s));
        }

        tokens
    }

    struct Parser {
        iter: std::iter::Peekable<std::vec::IntoIter<Token>>,
        op_stack: Vec<Token>,
        val_stack: Vec<Sym>,
    }

    impl Parser {
        fn term (&mut self) -> Result<Sym, Error> {
            match self.iter.next() {
                Some(Token::Str(s)) => {
                    if let Ok(n) = s.parse::<f32>() {
                        Ok(Sym::Val(n))
                    } else {
                        Ok(Sym::Var(s))
                    }
                },
                Some(Token::Minus) => {
                    Ok(self.term()?.negate())
                },
                Some(Token::Open) => {
                    let expr = self.expr()?;
                    if let Some(Token::Close) = self.iter.next() {
                        Ok(expr)
                    } else {
                        Err(parse_error("Unclosed parenthesis"))
                    }
                },
                // No other token can start a term
                _ => Err(parse_error("Invalid expression term"))
            }
        }

        fn pop_higher_than (&mut self, pre: Precedence) {
            loop {
                match self.op_stack.last() {
                    Some(op) => {
                        // if precedence is not higher
                        if op.precedence().unwrap() <= pre {
                            return;
                        }
                    },
                    // No ops left
                    None => {
                        return;
                    }
                }

                let op = self.op_stack.pop().unwrap();
                let right = self.val_stack.pop().unwrap();
                let left = self.val_stack.pop().unwrap();

                let result = match op {
                    Token::Plus => {
                        Sym::add(left, right)
                    },
                    Token::Minus => {
                        Sym::add(left, right.negate())
                    },
                    Token::Mul => {
                        Sym::mul(left, right)
                    },
                    Token::Div => {
                        Sym::mul(left, Sym::Inv(Box::new(right)))
                    },
                    op => {
                        panic!("{:?} is not an operator", op);
                    },
                };

                self.val_stack.push(result);
            }
        }

        fn push_term (&mut self) -> Result<(), Error> {
            let val = self.term()?;
            self.val_stack.push(val);
            Ok(())
        }

        fn expr (&mut self) -> Result<Sym, Error> {
            let saved = (
                std::mem::replace(&mut self.op_stack, vec![]),
                std::mem::replace(&mut self.val_stack, vec![]),
            );

            self.push_term()?;
            loop {
                match self.iter.peek() {
                    Some(tk) if tk.precedence().is_some() => {},
                    _ => { break; }
                }

                let op = self.iter.next().unwrap();
                self.pop_higher_than(op.precedence().unwrap());

                self.op_stack.push(op);
                self.push_term()?;
            }

            self.pop_higher_than(0);
            let result = self.val_stack.pop().unwrap();

            assert!(self.op_stack.is_empty());
            assert!(self.val_stack.is_empty());

            self.op_stack = saved.0;
            self.val_stack = saved.1;

            Ok(result)
        }
    }

    pub fn parse (input: &str) -> Result<Sym, Error> {
        let tokens = get_tokens(input);

        let mut parser = Parser {
            iter: tokens.into_iter().peekable(),
            op_stack: vec![],
            val_stack: vec![],
        };
        parser.expr()
    }

    #[test]
    fn test_parse () {
        assert_eq!(parse("2.0").unwrap(), Sym::Val(2.0));
        assert_eq!(parse("myvar").unwrap(), Sym::Var("myvar".to_string()));
        assert_eq!(parse("(1)").unwrap(), Sym::Val(1.0));
        assert_eq!(parse("1+2*3").unwrap(), Sym::Sum(vec![Sym::Val(1.0), Sym::Mul(vec![Sym::Val(2.0), Sym::Val(3.0)])]));
        assert_eq!(parse("1*2+3").unwrap(), Sym::Sum(vec![Sym::Mul(vec![Sym::Val(1.0), Sym::Val(2.0)]), Sym::Val(3.0)]));
    }
}

pub use parse::parse as parse_sym;