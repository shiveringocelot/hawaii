
use super::{symbol, Fold, Sym, VarTable, VarType, VarSrc};
use crate::scene::optimize::{self, Transformation as OptTransformation};

use cgmath::InnerSpace;

type Vec3 = cgmath::Vector3<f32>;

#[derive(Clone, Copy, Debug)]
pub enum Axis { X, Y, Z }
impl Axis {
	pub fn to_s (&self) -> &'static str {
		match self {
			Axis::X => "x",
			Axis::Y => "y",
			Axis::Z => "z",
		}
	}
}


#[derive(Clone)]
pub struct Scale (pub Sym);
impl Fold for Scale {
	fn invert (&mut self) {
		self.0 = Sym::Inv(Box::new(self.0.clone()));
	}

	fn transform (&self, p: &mut Vec3, scale: &mut f32, vars: &VarSrc<'_>) {
		let s = self.0.eval(vars);
		*p = p.map(|x| x*s);
		*scale *= s.abs();
	}

	fn shader_code (&self) -> String {
		format!("p *= {}; scale *= abs({});", self.0, self.0)
	}

	fn to_optimizable_transform (&self) -> Option<OptTransformation> {
		Some(OptTransformation::Scale(self.clone()))
	}
}

#[derive(Clone)]
pub struct Translate {
	pub x: Sym,
	pub y: Sym,
	pub z: Sym,
}
impl Fold for Translate {
	fn invert (&mut self) {
		self.x.negate_mut();
		self.y.negate_mut();
		self.z.negate_mut();
	}

	fn transform (&self, p: &mut Vec3, scale: &mut f32, vars: &VarSrc<'_>) {
		p.x += self.x.eval(vars);
		p.y += self.y.eval(vars);
		p.z += self.z.eval(vars);
	}

	fn shader_code (&self) -> String {
		format!("p += vec3({}, {}, {});", self.x, self.y, self.z)
	}

	fn to_optimizable_transform (&self) -> Option<OptTransformation> {
		Some(OptTransformation::Translate(self.clone()))
	}
}

#[derive(Clone)]
pub struct Rotate {
	pub x: Sym,
	pub y: Sym,
	pub z: Sym,
	pub angle: Sym,
}
impl Fold for Rotate {
	fn transform (&self, p: &mut Vec3, scale: &mut f32, vars: &VarSrc<'_>) { unimplemented!(); }

	fn invert (&mut self) {
		self.angle.negate_mut();
	}

	fn shader_code (&self) -> String {
		// It must be replaced by an optimized alternatives
		panic!("Rotate fold can't be compiled to shader code");
	}

	fn to_optimizable_transform (&self) -> Option<OptTransformation> {
		Some(OptTransformation::Rotate(self.clone()))
	}
}
impl Rotate {
	pub fn optimize (self, vars: &mut VarTable) -> Box<dyn Fold> {
		use Sym::Val as V;
		let axis = match (self.x, self.y, self.z) {
			(V(1.0), V(0.0), V(0.0)) => Some(Axis::X),
			(V(0.0), V(1.0), V(0.0)) => Some(Axis::Y),
			(V(0.0), V(0.0), V(1.0)) => Some(Axis::Z),
			_ => None,
		};
		if let Some(axis) = axis {
			Box::new(RotateAxis::new(axis, self.angle, vars))
		} else {
			unimplemented!()
		}
	}
}


#[derive(Clone)]
pub struct RotateAxis {
	axis: Axis,
	angle: Sym,

	matrix_index: usize,
	sub_vec: symbol::Field,
}
impl RotateAxis {
	pub fn new (axis: Axis, angle: Sym, vars: &mut VarTable) -> Self {
		let matrix_index = vars.set(
			VarType::Mat2,
			/*symbol.parse_sym(format!(
				"mat2(cos(), sin(), -sin(), cos())",
				angle, angle, angle, angle
			),*/
			Sym::Call("mat2".to_string(), vec![
				Sym::Call("cos".to_string(), vec![angle.clone()]),
				Sym::Call("sin".to_string(), vec![angle.clone()]),
				Sym::Call("sin".to_string(), vec![angle.clone()]).negate(),
				Sym::Call("cos".to_string(), vec![angle.clone()]),
			]),
		);

		let sub_vec = match axis {
			Axis::X => symbol::Field::YZ,
			Axis::Y => symbol::Field::XZ,
			Axis::Z => symbol::Field::XY,
		};

		Self { axis, angle, matrix_index, sub_vec }
	}
}
impl Fold for RotateAxis {
	fn transform (&self, p: &mut Vec3, scale: &mut f32, vars: &VarSrc<'_>) {
		let angle = self.angle.eval(vars);
		let c = angle.cos();
		let s = angle.sin();

		let (mut xr, mut yr) = match self.axis {
			Axis::X => (&mut p.y, &mut p.z),
			Axis::Y => (&mut p.x, &mut p.z),
			Axis::Z => (&mut p.x, &mut p.y),
		};

		let (x, y) = (*xr, *yr);
		*xr = x*c + -y*s;
		*yr = x*s + y*c;
	}

	fn invert (&mut self) {
		panic!("Optimized fold cannot be mutated");
	}

	fn shader_code (&self) -> String {
		format!("p.{} = var{} * p.{};", self.sub_vec, self.matrix_index, self.sub_vec)
	}
}

#[derive(Clone)]
pub struct MatrixTransform {
	matrix_index: usize,
}
impl MatrixTransform {
	pub fn new (matrix: optimize::Matrix, vars: &mut VarTable) -> Self {
		let expanded: Vec<Sym> = matrix.into_iter().flatten().collect();
		let matrix_index = vars.set(
			VarType::Mat4,
			Sym::Call("mat4".to_string(), expanded),
		);
		Self { matrix_index }
	}
}
impl Fold for MatrixTransform {
	fn transform (&self, p: &mut Vec3, scale: &mut f32, vars: &VarSrc<'_>) { unimplemented!(); }

	fn invert (&mut self) {
		panic!("Optimized fold cannot be mutated");
	}

	fn shader_code (&self) -> String {
		format!("p = (var{} * vec4(p, 1.0)).xyz;", self.matrix_index)
	}
}


#[derive(Clone)]
pub struct MirrorXYZ;
impl Fold for MirrorXYZ {
	fn transform (&self, p: &mut Vec3, scale: &mut f32, vars: &VarSrc<'_>) {
		*p = p.map(f32::abs);
	}
	fn shader_code (&self) -> String {
		format!("p = abs(p);")
	}
}

#[derive(Clone)]
pub struct MirrorAxis (pub Axis);
impl Fold for MirrorAxis {
	fn transform (&self, p: &mut Vec3, scale: &mut f32, vars: &VarSrc<'_>) {
		match self.0 {
			Axis::X => { p.x = p.x.abs() },
			Axis::Y => { p.y = p.y.abs() },
			Axis::Z => { p.z = p.z.abs() },
		}
	}
	fn shader_code (&self) -> String {
		format!("p.{} = abs(p.{});", self.0.to_s(), self.0.to_s())
	}
}

#[derive(Clone)]
pub struct MirrorPlane {
	pub x: Sym,
	pub y: Sym,
	pub z: Sym,
}
impl Fold for MirrorPlane {
	fn transform (&self, p: &mut Vec3, scale: &mut f32, vars: &VarSrc<'_>) {
		let n = Vec3::new(self.x.eval(vars), self.y.eval(vars), self.z.eval(vars));
		*p -= n * (2.0 * p.dot(n).min(0.0));
	}
	fn shader_code (&self) -> String {
		format!("mirrorFold(p, vec3({}, {}, {}));", self.x, self.y, self.z)
	}
}
