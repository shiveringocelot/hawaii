
mod symbol;
mod shapes;
mod folds;

mod sdf;

mod parse;
mod optimize;

use std::io::Write;
use std::io::Error as IOError;
use std::collections::HashMap;
use std::any::Any;

use dyn_clone::DynClone;

use shapes::*;
use folds::*;
use symbol::parse_sym;
use crate::params::ParamDef;

pub use symbol::Sym;

type Vec3 = cgmath::Vector3<f32>;

#[derive(Debug)]
pub enum Error {
	Io(IOError),
	ParseError(String, usize),
}

fn parse_error(s: impl ToString) -> Error { Error::ParseError(s.to_string(), 0) }

impl From<IOError> for Error {
	fn from (err: IOError) -> Error {
		Error::Io(err)
	}
}

pub trait Shape: DynClone + 'static {
	fn de (&self, p: Vec3, vars: &VarSrc<'_>) -> f32;
	fn shader_code (&self) -> String;

	fn to_item (self) -> Item where Self: Sized { Item::Shape(Box::new(self)) }
}

pub trait Fold: DynClone + 'static {
	fn transform (&self, p: &mut Vec3, scale: &mut f32, vars: &VarSrc<'_>);
	fn invert (&mut self) {}
	fn shader_code (&self) -> String;

	fn to_item (self) -> Item where Self: Sized { Item::Fold(Box::new(self)) }
	fn to_optimizable_transform (&self) -> Option<optimize::Transformation> {
		// Fuck it, I must clone. Rust compiler won't work without it...
		None
	}
}

dyn_clone::clone_trait_object!(Shape);
dyn_clone::clone_trait_object!(Fold);

#[derive(Clone)]
pub enum Item {
	Shape(Box<dyn Shape>),
	Fold(Box<dyn Fold>),
	Orbit { x: Sym, y: Sym, z: Sym },
	Loop { count: usize, items: Vec<Item> },
}

impl Item {
	fn invert (&mut self) {
		match self {
			Self::Fold(fold) => fold.invert(),
			Self::Loop { count, items } => {
				items.reverse();
				items.iter_mut().for_each(Item::invert);
			}
			Self::Shape(_) => {},
			Self::Orbit {..} => {},
		}
	}
}

pub struct VarSrc <'a> {
	params: &'a crate::Params,
	var_table: &'a VarTable,
}

impl <'a> VarSrc<'a> {
	fn get (&self, s: &str) -> Sym {
		for def in &self.params.defs {
			if let Some(s) = &def.name {
				return Sym::Val(self.params.values[def.index]);
			}
		}

		panic!("Unknown variable: {}", s);
	}
}

#[derive(Copy, Clone, Debug)]
pub enum VarType { Float, Mat2, Mat4 }

impl std::fmt::Display for VarType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		use std::fmt::Display;
    	let s = match self {
    		VarType::Float => "float",
    		VarType::Mat2 => "mat2",
    		VarType::Mat4 => "mat4",
    	};
		s.fmt(f)
    }
}


#[derive(Default)]
pub struct VarTable {
	vars: Vec<(VarType, Sym)>
}

impl VarTable {
	pub fn get (&self, index: usize) -> (VarType, &Sym) {
		let p = &self.vars[index];
		(p.0, &p.1)
	}

	pub fn set (&mut self, tp: VarType, val: Sym) -> usize {
		let ix = self.vars.len();
		self.vars.push((tp, val));
		ix
	}

	pub fn iter (&self) -> impl Iterator<Item=(usize, VarType, &Sym)> {
		self.vars.iter().enumerate().map(|(i, (t, v))| (i, *t, v))
	}
}

#[derive(Default)]
pub struct Scene {
	pub params: Vec<String>,

	/// Table of variable definitions
	var_table: VarTable,

	/// Items in the perspective of shapes.
	///
	/// This is how the user describes them in the scene file.
	parsed_items: Vec<Item>,

	/// Items in the sdf perspective and optimized
	///
	/// This is how they are written in the shader 
	sdf_items: Vec<Item>,
}

#[derive(Debug, Eq, PartialEq)]
enum CodeType { Sdf, Orbit }

impl Scene {
	pub fn sdf (&self, pos: Vec3, params: &crate::Params) -> f32 {
		let vars = VarSrc { params, var_table: &self.var_table };
		sdf::sdf(pos, &self.sdf_items, &vars)
	}

	pub fn parse_file (filename: &str) -> Result<(Self, Vec<ParamDef>), Error> {
		use std::io::BufRead;

		let file = std::fs::File::open(filename)?;
		let lines = std::io::BufReader::new(file).lines().collect::<Result<Vec<String>, IOError>>()?;

		let output = parse::parse_lines(lines)?;
		let mut scene = Scene::default();
		scene.parsed_items = output.items;
		scene.params = output.params.iter().map(|def| def.name.clone().unwrap()).collect();

		// The items as the user defined them without optimizations
		scene.sdf_items = scene.parsed_items.clone();
		scene.sdf_items.reverse();
		scene.sdf_items.iter_mut().for_each(Item::invert);

		optimize::optimize(&mut scene);

		Ok((scene, output.params))
	}

	fn code_body (&self, code_type: CodeType) -> String {
		let mut buf: Vec<u8> = vec![];

		struct Scope <'a> {
			iter: std::slice::Iter<'a, Item>,
			indent: String,
			dist_var: String,
			pos_var: String,
			bracketed: bool,
		}

		let mut scope_stack = vec![];
		let mut loop_counter = 0;

		fn incr (n: &mut usize) -> usize { *n += 1; *n-1 }

		let mut scope = Scope {
			iter: self.sdf_items.iter(),
			indent: "\t".to_string(),
			dist_var: "d".to_string(),
			pos_var: "p".to_string(),
			bracketed: false,
		};

		for (i, tp, val) in self.var_table.iter() {
			writeln!(buf, "\t{} var{} = {};", tp, i, val);
		}

		loop {
			if let Some(item) = scope.iter.next() {
				match item {
					Item::Shape(shape) => {
						if code_type == CodeType::Sdf {
							writeln!(buf, "d = {};", shape.shader_code());
						}
					},
					Item::Fold(fold) => {
						writeln!(buf, "{}", fold.shader_code());
					},
					Item::Orbit { x, y, z } => {
						if code_type == CodeType::Orbit {
							writeln!(buf, "orbit = min(orbit, abs(p - vec3({},{},{})));", x, y, z);
						}
					},
					Item::Loop { count, items } => {
						let ix = incr(&mut loop_counter);
						writeln!(buf, "for (int i{} = 0; i{} < {}; i{}++) {{", ix, ix, count, ix);

						let new_scope = Scope {
							iter: items.iter(),
							indent: scope.indent.clone(),
							dist_var: scope.dist_var.clone(),
							pos_var: scope.pos_var.clone(),
							bracketed: true,
						};
						scope_stack.push(std::mem::replace(&mut scope, new_scope));
					}
				} 
			} else {
				if let Some(next) = scope_stack.pop() {
					let last = std::mem::replace(&mut scope, next);
					if last.bracketed {
						writeln!(buf, "}}");
					}
				} else {
					break;
				}
			}
		}

		String::from_utf8(buf).unwrap()
	}

	pub fn uniforms_line (&self) -> String {
		let mut buf: Vec<u8> = vec![];
		for param in &self.params {
			writeln!(buf, "uniform float {};", param);
		}
		String::from_utf8(buf).unwrap()
	}

	pub fn shader_code (&self) -> String {
		let mut buf: Vec<u8> = vec![];
		writeln!(buf, "float sdf (vec3 p) {{");
		writeln!(buf, "\tfloat d = 0.0;");
		writeln!(buf, "\tfloat scale = 1.0;");

		writeln!(buf, "{}", self.code_body(CodeType::Sdf));

		writeln!(buf, "\treturn d / scale;");
		writeln!(buf, "}}");


		writeln!(buf, "vec3 color_field (vec3 p) {{");
		writeln!(buf, "\tvec3 orbit = vec3(1e20);");
		writeln!(buf, "\tfloat scale = 1.0;");

		writeln!(buf, "{}", self.code_body(CodeType::Orbit));

		writeln!(buf, "\treturn orbit;");
		writeln!(buf, "}}");

		String::from_utf8(buf).unwrap()
	}

}
