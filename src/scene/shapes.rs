
use cgmath::{InnerSpace};

use super::{Shape, Sym, VarSrc};

type Vec3 = cgmath::Vector3<f32>;

#[derive(Clone)]
pub struct Sphere(pub Sym);
impl Shape for Sphere {
	fn de (&self, p: Vec3, vars: &VarSrc<'_>) -> f32 {
		return p.magnitude() - self.0.eval(vars);
	}

	fn shader_code (&self) -> String {
		format!("length(p) - {}", self.0)
	}
}

#[derive(Clone)]
pub struct Cube(pub Sym);
impl Shape for Cube {
	fn de (&self, p: Vec3, vars: &VarSrc<'_>) -> f32 {
		let s = self.0.eval(vars);
    	let a = Vec3::new(p.x.abs()-s, p.y.abs()-s, p.z.abs()-s);
    	return a.x.max(a.y).max(a.z).min(0.0) + Vec3::new(a.x.max(0.0), a.y.max(0.0), a.z.max(0.0)).magnitude();
	}

	fn shader_code (&self) -> String {
		format!("boxShape(p, {})", self.0)
	}
}