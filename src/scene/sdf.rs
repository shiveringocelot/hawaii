
use crate::Vec3;

use super::{Item, VarSrc};

const MAX_STEPS: usize = 32;
const DEPTH: f32 = 32.0;
const EPSILON: f32 = 0.01;

struct State <'a> {
    p: Vec3,
    dist: f32,
    scale: f32,
    vars: &'a VarSrc<'a>,
}

fn sdf_inner (s: &mut State, items: &[Item]) {
    for item in items {
        match item {
            Item::Fold(fold) => {
                fold.transform(&mut s.p, &mut s.scale, &s.vars);
            },
            Item::Shape(shape) => {
                s.dist = shape.de(s.p, s.vars);
            },
            Item::Loop { count, items } => {
                for _ in 0..*count { sdf_inner(s, items); }
            },
            Item::Orbit { .. } => {}
        }
    }
}

pub fn sdf (mut p: Vec3, items: &[Item], vars: &VarSrc<'_>) -> f32 {
    let mut state = State { p, scale: 1.0, dist: 0.0, vars };
    sdf_inner(&mut state, items);
    return state.dist / state.scale;
}