use super::*;

pub type Matrix = [[Sym; 4]; 4];

pub enum Transformation {
	Translate(Translate),
	Rotate(Rotate),
	Scale(Scale),
}

enum ScopeType {
	Top,
	Loop { count: usize },
}

struct Scope {
	// Accumulated transformations
	transforms: Vec<Transformation>,
	iter: std::vec::IntoIter<Item>,
	items: Vec<Item>,

	// Loop where the scope comes from
	scope_type: ScopeType,
}

fn sum (xs: &[&Sym]) -> Sym {
	Sym::Sum(xs.iter().map(|x| (*x).clone()).collect())
}

fn mul (xs: &[&Sym]) -> Sym {
	Sym::Mul(xs.iter().map(|x| (*x).clone()).collect())
}

fn apply_transformation (mat: &mut Matrix, t: Transformation) {
	let mut tmat = Matrix::default();
	tmat[3][3] = Sym::Val(1.0);
	match t {
		Transformation::Scale(t) => {
			tmat[0][0] = t.0.clone();
			tmat[1][1] = t.0.clone();
			tmat[2][2] = t.0.clone();
		},
		Transformation::Translate(t) => {
			tmat[0][0] = Sym::Val(1.0);
			tmat[1][1] = Sym::Val(1.0);
			tmat[2][2] = Sym::Val(1.0);

			tmat[3][0] = t.x;
			tmat[3][1] = t.y;
			tmat[3][2] = t.z;
		},
		Transformation::Rotate(t) => {
			// bruh
			// https://en.wikipedia.org/wiki/Rotation_matrix#Rotation_matrix_from_axis_and_angle

			let cos = Sym::Call("cos".to_string(), vec![t.angle.clone()]);
			let sin = Sym::Call("sin".to_string(), vec![t.angle.clone()]);
			let icos = Sym::add(Sym::Val(1.0), cos.clone().negate());

			tmat[0][0] = sum(&[&cos, &mul(&[&t.x, &t.x, &icos])]);
			tmat[0][1] = sum(&[&mul(&[&t.y, &t.x, &icos]), &mul(&[&t.z, &sin])]);
			tmat[0][2] = sum(&[&mul(&[&t.z, &t.x, &icos]), &mul(&[&t.y, &sin]).negate()]);

			tmat[1][0] = sum(&[&mul(&[&t.x, &t.y, &icos]), &mul(&[&t.z, &sin]).negate()]);
			tmat[1][1] = sum(&[&cos, &mul(&[&t.y, &t.y, &icos])]);
			tmat[1][2] = sum(&[&mul(&[&t.z, &t.y, &icos]), &mul(&[&t.x, &sin])]);

			tmat[2][0] = sum(&[&mul(&[&t.x, &t.z, &icos]), &mul(&[&t.y, &sin])]);
			tmat[2][1] = sum(&[&mul(&[&t.y, &t.z, &icos]), &mul(&[&t.x, &sin]).negate()]);
			tmat[2][2] = sum(&[&cos, &mul(&[&t.z, &t.z, &icos])]);

			// Managing to transcribe that is an achievemnt
		},
	}

	let mut dst = Matrix::default();
	for i in 0..4 {
		for j in 0..4 {
			let mut terms = vec![];
			for k in 0..4 {
				terms.push(mul(&[&tmat[i][k], &mat[k][j]]));
			}

			dst[i][j] = Sym::Sum(terms);
		}
	}

	*mat = dst;
}

pub fn optimize (scene: &mut Scene) {

	impl Scope {
		fn push_transforms (&mut self, scene: &mut Scene) {
			let mut iter = self.transforms.drain(..);
			match (iter.next(), iter.next()) {
				(None, _) => {},
				(Some(Transformation::Scale(t)), None) => {
					self.items.push(t.to_item());
				},
				(Some(Transformation::Translate(t)), None) => {
					self.items.push(t.to_item());
				},
				(Some(Transformation::Rotate(r)), None) => {
					self.items.push(Item::Fold(r.optimize(&mut scene.var_table)));
				},
				(Some(a), Some(b)) => {
					let mut matrix = Matrix::default();
					matrix[0][0] = Sym::Val(1.0);
					matrix[1][1] = Sym::Val(1.0);
					matrix[2][2] = Sym::Val(1.0);
					matrix[3][3] = Sym::Val(1.0);

					for t in [a, b].into_iter().chain(iter) {
						apply_transformation(&mut matrix, t);
					}

					for col in matrix.iter_mut() {
						for sym in col.iter_mut() {
							sym.optimize_mut();
						}
					}

					self.items.push(MatrixTransform::new(matrix, &mut scene.var_table).to_item());
				}
			}
		}
	}

	let mut scope_stack = vec![];

	// Take the original list and make a new list
	let original = std::mem::replace(&mut scene.sdf_items, vec![]);

	let mut scope = Scope {
		iter: original.into_iter(),
		// Accumulated transforms, for later optimization
		transforms: vec![],
		items: vec![],
		scope_type: ScopeType::Top,
	};

	loop {
		if let Some(item) = scope.iter.next() {
			match item {
				Item::Fold(fold) => {
					match fold.to_optimizable_transform() {
						Some(t) => {
							// uncomment for no matrix transform optimization
							scope.push_transforms(scene);
							scope.transforms.push(t);
						},
						None => {
							scope.push_transforms(scene);
							scope.items.push(Item::Fold(fold));
						}
					}
				},
				Item::Loop { count, items } => {
					scope.push_transforms(scene);
					let new_scope = Scope {
						iter: items.into_iter(),
						// Accumulated transforms, for later optimization
						transforms: vec![],
						items: vec![],
						scope_type: ScopeType::Loop { count },
					};

					let prev = std::mem::replace(&mut scope, new_scope);
					scope_stack.push(prev);
				},
				item => {
					scope.push_transforms(scene);
					scope.items.push(item);
				}
			}
		} else {
			scope.push_transforms(scene);

			match scope.scope_type {
				ScopeType::Top => {
					scene.sdf_items = scope.items;
					return;
				},
				ScopeType::Loop { count } => {
					let parent = scope_stack.pop().unwrap();
					let loop_scope = std::mem::replace(&mut scope, parent);			
					let loop_item = Item::Loop { count, items: loop_scope.items };
					scope.items.push(loop_item);
				}
			}
		}
	}
}