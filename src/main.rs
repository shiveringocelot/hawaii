#![allow(warnings)]

#[macro_use]
extern crate glium;

mod scene;
mod params;
mod camera;
mod color;
mod shader;
mod text;

use std::time::{Instant, Duration};
use std::cell::RefCell;

use glium::{Surface, Display, Version, Api, uniforms::UniformValue, texture::Texture3d};
use glium::glutin::{self, window::Window};
use glium::glutin::event_loop::{EventLoop, ControlFlow};
use glium::glutin::event::{Event, WindowEvent, VirtualKeyCode, ElementState, KeyboardInput};
use glium::index::PrimitiveType;

use cgmath::{Matrix4, Vector3, Zero, One, Basis3, Rotation3, Rotation, Rad, MetricSpace, InnerSpace, Transform};

use scene::Scene;
use params::Params;
use camera::Camera;
use shader::Shader;
use text::Text;
use color::{Color, Colors};

type Vec3 = Vector3<f32>;

const FPS: u32 = 60;
const LIGHT_DIR: Vec3 = Vec3::new(2.0, 3.0, -1.0);
const PRECOMPUTE_RESOLUTION: u32 = 256;

#[derive(Default)]
struct Input {
    mouse_captured: bool,
    last_mouse_pos: (i32, i32),

    front: bool,
    back: bool,
    left: bool,
    right: bool,
    up: bool,
    down: bool,

    plus: bool,
    minus: bool,

    param_index: usize,
}

struct Uniforms <'a> {
    app: &'a App,
    precomp_texture: Option<&'a glium::texture::SrgbTexture2d>,
}

#[derive(Default)]
struct MovingAvg {
    samples: [f32; 32],
    index: usize,
    avg: f32,
}

impl MovingAvg {
    fn add (&mut self, v: f32) {
        self.samples[self.index] = v;
        self.index = (self.index + 1) % 32;
        self.avg = self.samples.iter().cloned().sum::<f32>() / 32.0;
    }
}

pub struct Rand(u32);
impl Rand {
    pub fn new () -> Self {
        let mut r = Rand(std::time::UNIX_EPOCH.elapsed().unwrap().as_millis() as u32);
        r.next(); r.next(); r
    }

    pub fn next (&mut self) -> f32 {
        self.0 = self.0.wrapping_mul(1103515245).wrapping_add(12345);
        self.0 as f32 / u32::MAX as f32
    }
}

struct App {
    scene_file: String,
    scene: Scene,
    camera: Camera,
    input: Input,

    last_frame: Instant,

    render_time: RefCell<MovingAvg>,
    text: Text,
    shader: Shader,
    display: Display,
    draw_fn: Box<dyn Fn(&App)>,

    rand: Rand,
    params: Params,
    colors: Colors,
    surface_color_texture: Texture3d,

    debug: bool,
}

impl App {
    fn draw (&self) {
        let start = Instant::now();
        (self.draw_fn)(self);
        let dur = start.elapsed();

        self.render_time.borrow_mut().add(dur.as_secs_f32());
    }

    fn resized (&mut self) {
        let size = self.window().inner_size();
        self.camera.aspect = size.width as f32 / size.height as f32;
        self.text.set_size(size.width as f32, size.height as f32);
    }

    fn update_uniforms (&mut self) {
        //self.camera.write_to_uniforms(&mut self.uniforms);
    }

    fn window (&self) -> impl std::ops::Deref<Target=Window> + '_ {
        std::cell::Ref::map(self.display.gl_window(), |r| r.window())
    }

    fn rand (&mut self) -> f32 {
        self.rand.next()
    }

    fn update_frame (&mut self) {
        let now = Instant::now();
        let delta = now.duration_since(self.last_frame).as_secs_f32();

        let mut dir = Vec3::zero();
        if self.input.up { dir.y += 1.0; }
        if self.input.down { dir.y -= 1.0; }
        if self.input.right { dir.x += 1.0; }
        if self.input.left { dir.x -= 1.0; }
        if self.input.front { dir.z += 1.0; }
        if self.input.back { dir.z -= 1.0; }

        if self.input.plus {
            self.params.add(self.input.param_index, delta * 3.0);
        }
        if self.input.minus {
            self.params.add(self.input.param_index, -delta * 3.0);
        }

        self.params.next_frame(delta, &mut self.rand);
        // self.camera.next_frame(delta, &self.scene, &self.params, &mut self.rand);

        self.colors.next_frame(delta, &self.params);
        self.surface_color_texture = self.colors.create_texture(&self.display);

        self.camera.translate(dir * delta);
        self.update_uniforms();

        let sdf = self.scene.sdf(self.camera.position, &self.params);
        let dir = self.camera.matrix.transform_vector(Vec3::unit_z());
        self.text.set_text(&[
            format!("FPS: {:.0}", 1.0 / self.render_time.borrow().avg),
            format!("Position: {:.2}, {:.2}, {:.2}",
                self.camera.position.x,
                self.camera.position.y,
                self.camera.position.z,
            ),
            format!("SDF: {:.2}", sdf),
            format!("Gradient: {:.2}", self.camera.gradient),
            format!("Panic: {:?}", self.camera.panic),
            {
                let (name, val) = self.params.get_name_val(self.input.param_index as usize);
                format!("Param {}: {:.3}", name, val)
            },
        ]);

        self.last_frame = now;
    }
}

// Input
impl App {
    fn capture_mouse (&mut self, capture: bool) {
        self.input.mouse_captured = capture;
        let window = self.window();
        window.set_cursor_grab(capture);
        window.set_cursor_visible(!capture);
    }

    fn mouse_move (&mut self, mut x: i32, mut y: i32) {
        // Cancel processing
        if (!self.input.mouse_captured) {
            self.input.last_mouse_pos = (x, y);
            return;
        }

        let size = self.window().inner_size();
        let center = (size.width as i32/2, size.height as i32/2);

        // This most likely just means that this is just a reset by a previous mouse_move
        if (x == center.0 && y == center.1) {
            self.input.last_mouse_pos = (x, y);
            return;
        }

        let last = self.input.last_mouse_pos;
        self.input.last_mouse_pos = (x, y);

        x -= last.0;
        y -= last.1;

        self.camera.yaw += x as f32 / size.width as f32;
        self.camera.pitch += y as f32 / size.height as f32;
        self.camera.calculate_matrix();

        self.update_uniforms();

        let npos = glutin::dpi::PhysicalPosition::new(size.width/2, size.height/2);
        // We don't mind the error
        let _ = self.window().set_cursor_position(npos);
    }

    fn reset_inputs (&mut self) {
        let last_mouse_pos = self.input.last_mouse_pos;
        let param_index = self.input.param_index;

        self.input = Input {
            last_mouse_pos, param_index,
            .. Default::default()
        };
    }

    fn reload_scene (&mut self) {
        match scene::Scene::parse_file(&self.scene_file) {
            Ok((scene, sdf_params)) => {
                self.scene = scene;
                let mut params = Params::new(sdf_params, &mut self.rand);
                params.copy_from(&self.params);
                self.params = params;
                self.shader.reload(&self.display, &self.scene);
            },
            Err(err) => {
                println!("Error reading scene: {:?}", err);
            }
        }
    }
}

impl <'a> glium::uniforms::Uniforms for Uniforms <'a> {
    fn visit_values<'b, F: FnMut(&str, UniformValue<'b>)>(&'b self, mut f: F) {
        f("cameraPosition", UniformValue::Vec3(self.app.camera.position.into()));
        f("cameraMatrix", UniformValue::Mat4(self.app.camera.matrix.into()));
        f("aspectRatio", UniformValue::Float(self.app.camera.aspect));
        f("lightDirection", UniformValue::Vec3(LIGHT_DIR.normalize().into()));

        for def in self.app.params.sdf_defs.iter() {
            if let Some(name) = &def.name {
                f(name, UniformValue::Float(self.app.params.values[def.index]));
            }
        }

        f("uBgColor", UniformValue::Vec3(self.app.colors.bg.to_rgb()));
        f("uGlowColor", UniformValue::Vec3(self.app.colors.glow.to_rgb()));
        f("uSurfaceColorSpace", UniformValue::Texture3d(
            &self.app.surface_color_texture,
                Some(glium::uniforms::SamplerBehavior {
                wrap_function: {
                    let wrap = glium::uniforms::SamplerWrapFunction::Clamp;
                    (wrap, wrap, wrap)
                },
                minify_filter: glium::uniforms::MinifySamplerFilter::Linear,
                magnify_filter: glium::uniforms::MagnifySamplerFilter::Linear,
                depth_texture_comparison: None,
                max_anisotropy: 1,
            })
        ));


        if let Some(tex) = self.precomp_texture {
            f("precomputed", UniformValue::SrgbTexture2d(tex, Some(glium::uniforms::SamplerBehavior {
                wrap_function: {
                    let wrap = glium::uniforms::SamplerWrapFunction::Clamp;
                    (wrap, wrap, wrap)
                },
                minify_filter: glium::uniforms::MinifySamplerFilter::Nearest,
                magnify_filter: glium::uniforms::MagnifySamplerFilter::Nearest,
                depth_texture_comparison: None,
                max_anisotropy: 1,
            })));

        }
    }
}

struct UpdateFrameEvent;

fn main() {
    let event_loop = EventLoop::with_user_event();
    let display = glium::Display::new(
        glutin::window::WindowBuilder::new().with_title("Hawaii"),
        glutin::ContextBuilder::new(),
        &event_loop
    ).unwrap();

    let glversion = *display.get_opengl_version();
    let glapi = match glversion {
        Version(Api::Gl, _, _) => "OpenGL",
        Version(Api::GlEs, _, _) => "OpenGL ES"
    };

    println!("{} version: {}\n", glapi, display.get_opengl_version_string());

    if !glium::texture::is_texture_3d_supported(&display) {
        panic!("No 3d texture support");
    }

    let scene_file = std::env::args().nth(1).unwrap_or("scenes/example.scene".to_string());

    let mut rand = Rand::new();
    let (scene, sdf_params) = scene::Scene::parse_file(&scene_file).unwrap();
    let params = Params::new(sdf_params, &mut rand);

    let shader = Shader::create(&display, &scene);

    let colors = Colors::new_rand(&mut rand);


    // building the vertex buffer, which contains all the vertices that we will draw
    let vertex_buffer = {
        #[derive(Copy, Clone)]
        struct Vertex {
            position: [f32; 2],
        }

        implement_vertex!(Vertex, position);

        glium::VertexBuffer::new(&display,
            &[
                Vertex { position: [-1.0, -1.0] },
                Vertex { position: [-1.0,  1.0] },
                Vertex { position: [ 1.0,  1.0] },
                Vertex { position: [ 1.0, -1.0] },
            ]
        ).unwrap()
    };

    // building the index buffer
    let index_buffer = glium::IndexBuffer::new(&display, PrimitiveType::TrianglesList,
                                               &[0u16, 1, 2, 0, 2, 3]).unwrap();

    let precomp_texture = glium::texture::SrgbTexture2d::empty_with_format(
        &display,
        glium::texture::SrgbFormat::U8U8U8,
        glium::texture::MipmapsOption::NoMipmap,
        PRECOMPUTE_RESOLUTION,
        PRECOMPUTE_RESOLUTION,
    ).unwrap();

    let mut app = App {
        surface_color_texture: colors.create_texture(&display),

        scene_file, scene, params, rand, colors,

        camera: Camera::new(),
        input: Default::default(),
        last_frame: Instant::now(),
        render_time: Default::default(),
        debug: true,

        text: Text::try_new(&display).unwrap(),
        // surface_color_texture: Texture3d::empty(&display, 1, 1, 1).unwrap(),

        shader, display,
        draw_fn: Box::new(move |app| {
            // Draw the precomputed texture
            {
                let uniforms = Uniforms { app: &app, precomp_texture: None };

                let mut target = glium::framebuffer::SimpleFrameBuffer::new(&app.display, &precomp_texture).unwrap();
                target.clear_color(0.0, 0.0, 0.0, 0.0);
                target.draw(&vertex_buffer, &index_buffer, &app.shader.pc_program, &uniforms, &Default::default()).unwrap();
            }

            let uniforms = Uniforms { app: &app, precomp_texture: Some(&precomp_texture) };

            // drawing a frame
            let mut target = app.display.draw();
            target.clear_color(0.0, 0.0, 0.0, 0.0);
            target.draw(&vertex_buffer, &index_buffer, &app.shader.px_program, &uniforms, &Default::default()).unwrap();
            if app.debug {
                app.text.draw(&mut target);
            }
            target.finish().unwrap();
        }),
    };

    app.resized();
    app.update_uniforms();

    app.draw();

    let event_proxy = event_loop.create_proxy();
    std::thread::spawn(move || {
        const DELAY: Duration = Duration::from_millis(1000 / FPS as u64);

        while let Ok(()) = event_proxy.send_event(UpdateFrameEvent) {
            std::thread::sleep(DELAY);
        }
    });

    // the main loop
    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Wait;

        match event {
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::CloseRequested => {
                    *control_flow = ControlFlow::Exit;
                },
                WindowEvent::Resized(..) => {
                    app.resized();
                    app.update_uniforms();
                },
                WindowEvent::Focused(false) => {
                    app.reset_inputs();
                    app.capture_mouse(false);
                },
                WindowEvent::CursorMoved { position, .. } => {
                    app.mouse_move(position.x as i32, position.y as i32);
                }
                WindowEvent::MouseInput {
                    state: ElementState::Pressed,
                    ..
                } => {
                    app.capture_mouse(true);
                },
                WindowEvent::KeyboardInput {
                    input: KeyboardInput {
                        state, virtual_keycode: Some(key), ..
                    },
                    ..
                } => {
                    let captured = app.input.mouse_captured;
                    match (key, state) {
                        (VirtualKeyCode::Q | VirtualKeyCode::Escape, ElementState::Pressed) => {
                            if app.input.mouse_captured {
                                app.capture_mouse(false);
                                app.reset_inputs();
                            } else {
                                //*control_flow = ControlFlow::Exit;
                            }
                        },
                        (VirtualKeyCode::W | VirtualKeyCode::Up, state) if captured => {
                            app.input.front = state == ElementState::Pressed;
                        },
                        (VirtualKeyCode::A | VirtualKeyCode::Left, state) if captured => {
                            app.input.left = state == ElementState::Pressed;
                        },
                        (VirtualKeyCode::S | VirtualKeyCode::Down, state) if captured => {
                            app.input.back = state == ElementState::Pressed;
                        },
                        (VirtualKeyCode::D | VirtualKeyCode::Right, state) if captured => {
                            app.input.right = state == ElementState::Pressed;
                        },

                        (VirtualKeyCode::E, state) if captured => {
                            app.input.up = state == ElementState::Pressed;
                        },
                        (VirtualKeyCode::F, state) if captured => {
                            app.input.down = state == ElementState::Pressed;
                        },


                        (VirtualKeyCode::R, ElementState::Pressed) => {
                            app.reload_scene();
                        },
                        (VirtualKeyCode::X, ElementState::Pressed) => {
                            app.debug = !app.debug;
                        },

                        (VirtualKeyCode::Period, state) => {
                            app.input.plus = state == ElementState::Pressed;
                        },
                        (VirtualKeyCode::Comma, state) => {
                            app.input.minus = state == ElementState::Pressed;
                        },

                        (VirtualKeyCode::Key1, ElementState::Pressed) => {
                            app.input.param_index = 0;
                        },
                        (VirtualKeyCode::Key2, ElementState::Pressed) => {
                            app.input.param_index = 1;
                        },
                        (VirtualKeyCode::Key3, ElementState::Pressed) => {
                            app.input.param_index = 2;
                        },
                        (VirtualKeyCode::Key4, ElementState::Pressed) => {
                            app.input.param_index = 3;
                        },
                        (VirtualKeyCode::Key5, ElementState::Pressed) => {
                            app.input.param_index = 4;
                        },
                        (VirtualKeyCode::Key6, ElementState::Pressed) => {
                            app.input.param_index = 5;
                        },
                        (VirtualKeyCode::Key7, ElementState::Pressed) => {
                            app.input.param_index = 6;
                        },
                        (VirtualKeyCode::Key8, ElementState::Pressed) => {
                            app.input.param_index = 7;
                        },
                        (VirtualKeyCode::Key9, ElementState::Pressed) => {
                            app.input.param_index = 8;
                        },
                        _ => {}
                    }
                },
                _ => {}
            },
            Event::UserEvent(UpdateFrameEvent) => {
                app.update_frame();
            },
            Event::MainEventsCleared => {
                app.window().request_redraw();
            },
            Event::RedrawRequested(_) => {
                app.draw();
            },
            _ => {}
        };
    });
}