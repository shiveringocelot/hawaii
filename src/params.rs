
// The parameters are modeled as a particle moving through an N-dimensional space
// with multiple forces acting on it.
// Read Params::next_step for details.

use crate::Rand;

const BROWN_STRENGTH: f32 = 0.2;
const SPEED: f32 = 0.01;

#[derive(Debug, Default, Clone)]
pub struct ParamDef {
    pub index: usize,
    pub name: Option<String>,

    pub mul: f32,
    pub sum: f32,
    pub force: f32,
    pub repeat: bool,
}

pub struct Params {
    pub defs: Vec<ParamDef>,
	pub sdf_defs: Vec<ParamDef>,

    // bg, glow, base, 3 orbits
    pub color_defs: [ParamDef; 6],

    /// Interpolated values between sample[1] and sample[2]
	pub values: Vec<f32>,

    /// Velocity at sample[3]
    deltas: Vec<f32>,

    /// History of positions, for a bilinear interpolation
    samples: [Vec<f32>; 4],

    /// Relative position in between the samples
    t: f32,
}

impl Params {
    pub fn new (mut sdf_defs: Vec<ParamDef>, rand: &mut Rand) -> Self {
        let color_defs: [ParamDef; 6] = (0..6).map(|i| ParamDef {
            index: sdf_defs.len() + i,
            name: None,
            mul: 1.0,
            sum: 0.0,
            force: 1.0,
            repeat: true,
        }).collect::<Vec<_>>().try_into().unwrap();

        let mut defs = sdf_defs.clone();
        defs.extend(color_defs.clone());

        let vals: Vec<f32> = defs.iter().map(|def| {
            rand.next() * def.mul + def.sum
        }).collect();

        let mut params = Params {
            samples: [vals.clone(), vals.clone(), vals.clone(), vals.clone()],
            values: vals,
            deltas: vec![0.0; defs.len()],
            sdf_defs, color_defs, defs,
            t: 0.0,
        };
        // Fill up the sample list with random data
        params.next_sample(rand);
        params.next_sample(rand);
        params.next_sample(rand);
        params
    }

    pub fn get_name_val (&self, index: usize) -> (String, f32) {
        (
            self.defs.get(index)
                .map(|d| d.name.clone())
                .flatten()
                .unwrap_or(String::new()),
            self.values.get(index)
                .cloned()
                .unwrap_or(0.0)
        )
    }

    pub fn add (&mut self, index: usize, mut val: f32) {
        if index < self.values.len() {
            val *= SPEED * self.defs[index].force;
            self.values[index] += val;
            for i in 0..4 {
                self.samples[i][index] += val;
            }
        }
    }

    pub fn copy_from (&mut self, other: &Self) {
        let len = self.values.len().min(other.values.len());
        self.values[0..len].copy_from_slice(&other.values[0..len])
    }

    pub fn next_frame (&mut self, delta: f32, rand: &mut Rand) {
        self.t += delta;
        while self.t > 1.0 {
            self.t -= 1.0;
            self.next_sample(rand);
        }

        // Sample the position with cubic interpolation
        // http://www.paulbourke.net/miscellaneous/interpolation/
        let x = self.t;
        for i in 0..self.values.len() {
            let y0 = self.samples[0][i];
            let y1 = self.samples[1][i];
            let y2 = self.samples[2][i];
            let y3 = self.samples[3][i];

            let x2 = x*x;
            let a0 = -0.5*y0 + 1.5*y1 - 1.5*y2 + 0.5*y3;
            let a1 = y0 - 2.5*y1 + 2.0*y2 - 0.5*y3;
            let a2 = -0.5*y0 + 0.5*y2;
            let a3 = y1;

            let mut y = (a0*x*x2 + a1*x2 + a2*x + a3);

            let def = &self.defs[i];
            if (def.repeat) {
                y = (y - def.sum) * def.mul;
                y = y.rem_euclid(1.0);
                y = y * def.mul + def.sum;
            }
            self.values[i] = y;
        }
    }

    /// Calculate and push the next position sample
    fn next_sample (&mut self, rand: &mut Rand) {
        // Use the last sample as base
        let mut values = self.samples[3].clone();

        let repulsion_count = self.defs.iter().filter(|def| !def.repeat).count() as i32;

        // Calculate field forces and apply them to the particle velocity
        for def in &self.defs {

            // Brownian force.
            // Pushes the particle in random directions
            let brown = (rand.next()*2.0 - 1.0) * BROWN_STRENGTH;

            // Only add repulsion force to the parameters that don't wrap around
            let repulsion = if def.repeat { 0.0 } else {
                // Field repulsion force.
                // Pushes the particle towards the center if it's close to the boundary
                let mut val = values[def.index];
                // Value relative to the range: 0..1
                val = (val - def.sum) / def.mul;
                // Center value in 0: -1..1
                val = val*2.0 - 1.0;

                // Make repulsion exponentially stronger when close to the boundary.
                // The higher the number of dimensions, the stronger the combined
                // repulsion acts over the particle, so we will exagerate the slope.
                val.powi(1 + repulsion_count*2)
            };

            self.deltas[def.index] += brown - repulsion;
        }

        // Keep the particle's velocity constant by normalizing it, regardless of the
        // forces acting on it
        let length = self.deltas.iter().cloned().map(|x| x*x).sum::<f32>().sqrt();
        for x in &mut self.deltas { *x /= length; }

        // TODO: Apply axis bias/resistance

        // Move the values according to the current velocity
        for def in &self.defs {
            let mut val = values[def.index];
            val += self.deltas[def.index] * SPEED * def.force;
            values[def.index] = val;
        }

        // Push the sample
        for i in (0..4).rev() {
            std::mem::swap(&mut self.samples[i], &mut values);
        }
    }
}