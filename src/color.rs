
use std::mem::swap;
use std::cmp::Ordering;

use glium::{Display, texture::{Texture3d, RawImage3d, ClientFormat}};
use crate::Rand;

#[derive(Default, Copy, Clone, Debug)]
pub struct Color {
    pub h: f32,
    pub s: f32,
    pub v: f32,
}

const TEXTURE_SIZE: usize = 4;

impl Color {
    pub fn new (h: f32, s: f32, v: f32) -> Self {
        Color { h, s, v }
    }

    pub fn rand_h (rand: &mut Rand, s: f32, v: f32) -> Self {
        Color::new(rand.next(), s, v)
    }

    pub fn to_rgb (&self) -> [f32; 3] {
        // https://en.wikipedia.org/wiki/HSL_and_HSV#HSV_to_RGB
        let c = self.v * self.s;
        let h = self.h.rem_euclid(1.0) * 6.0;
        let x = c * (1.0 - (h % 2.0 - 1.0).abs());
        let (r, g, b) = match h as i32 {
            0 => (c, x, 0.0),
            1 => (x, c, 0.0),
            2 => (0.0, c, x),
            3 => (0.0, x, c),
            4 => (x, 0.0, c),
            5 => (c, 0.0, x),
            _ => (0.0, 0.0, 0.0),
        };
        let m = self.v - c;
        [r+m, g+m, b+m]
    }
}

#[derive(Default)]
pub struct Colors {
    // Background
    pub bg: Color,

    // Glow
    pub glow: Color,

    // Surface base
    pub base: Color,

    // Surface orbit hues
    pub a: f32,
    pub b: f32,
    pub c: f32,
}

#[derive(PartialEq,PartialOrd)]
struct NonNan(f32);
impl NonNan {
    fn new(val: f32, or: f32) -> NonNan {
        if val.is_nan() {
            NonNan(or)
        } else {
            NonNan(val)
        }
    }
}
impl Eq for NonNan {}
impl Ord for NonNan {
    fn cmp(&self, other: &NonNan) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

#[derive(Clone, Copy)]
struct HueVal {
    hue: f32,
    val: f32,
}

fn mix_hues (mut hues: Vec<HueVal>) -> HueVal {
    // Normalize all the values, considering they might be negative so % is not good
    for hv in &mut hues { hv.hue = hv.hue.rem_euclid(1.0); }
    hues.sort_by_key(|x| NonNan::new(x.hue, f32::INFINITY));

    let val_sum = hues.iter().map(|o| o.val).sum::<f32>();
    if val_sum == 0.0 {
        // Just a simple average
        let hue = hues.iter().map(|o| o.hue).sum::<f32>() / hues.len() as f32;
        return HueVal { hue, val: 0.0 };
    }

    // All the different combinations of huevals where the first n values
    // are pushed over 1 (360 degrees)
    let shifts: Vec<_> = (0..hues.len()).map(|i| {
        let mut ls = hues.clone();
        for x in ls.iter_mut().take(i) { x.hue += 1.0; }
        ls
    }).collect();

    // Out of the shifts, select the one with the least variance
    let (_, avg_hue, variance) = shifts.into_iter()
        .map(|ls| {
            // weighted average
            let avg = ls.iter().map(|o| o.hue*o.val).sum::<f32>() / val_sum;
            let variance = ls.iter().map(|o| (o.hue-avg).powi(2) * o.val).sum();
            (ls, avg, variance)
        })
        .min_by_key(|(_, _, var)| NonNan::new(*var, f32::INFINITY))
        .unwrap();

    // The values were normalized and maybe pushed over 1, so modulo is enough
    HueVal { hue: avg_hue % 1.0, val: val_sum }
}

impl Colors {
    pub fn new_rand (rand: &mut Rand) -> Self {
        let mut c = Colors::default(); c.rand(rand); c
    }

    pub fn rand (&mut self, rand: &mut Rand) {
        self.bg = Color::rand_h(rand, 1.0, 0.01);
        self.glow = Color::rand_h(rand, 1.0, 0.25);
        self.base = Color::rand_h(rand, 1.0, 0.1);
        self.a = rand.next();
        self.b = rand.next();
        self.c = rand.next();
    }

    pub fn next_frame(&mut self, delta: f32, params: &crate::Params) {
        let colors = [&mut self.bg.h, &mut self.glow.h, &mut self.base.h, &mut self.a, &mut self.b, &mut self.c];
        for i in 0..6 {
            *colors[i] = params.values[params.color_defs[i].index];
        }
    }

    pub fn get_orbit_color (&self, a: f32, b: f32, c: f32) -> Color {
        let hvs = vec![
            HueVal { val: a, hue: self.a },
            HueVal { val: b, hue: self.b },
            HueVal { val: c, hue: self.c },
        ];

        let hv = mix_hues(hvs);

        if hv.val < 1.0 {
            return Color::new(hv.hue, 1.0, hv.val);

            let val = self.base.v + hv.val * (1.0 - self.base.v);
            let hv = mix_hues(vec![hv, HueVal { hue: self.base.h, val: 1.0 - hv.val }]);
            Color::new(hv.hue, 1.0, val)
        } else {
            let sat = 1.0 - (hv.val*0.5-0.5).powi(2);
            // Push the value over 1.0 as well, so that it shines under the shadow
            let val = 1.0 + (hv.val - 1.0) / 2.0;
            Color::new(hv.hue, sat, val)
        }
    }

    pub fn create_texture (&self, display: &Display) -> Texture3d {
        let mut data = vec![0u8; TEXTURE_SIZE.pow(3) * 3];
        for (i, px) in data.chunks_mut(3).enumerate() {
            let x = i % TEXTURE_SIZE;
            let y = (i / TEXTURE_SIZE) % TEXTURE_SIZE;
            let z = i / (TEXTURE_SIZE * TEXTURE_SIZE);

            let a = x as f32 / TEXTURE_SIZE as f32;
            let b = y as f32 / TEXTURE_SIZE as f32;
            let c = z as f32 / TEXTURE_SIZE as f32;

            let color = self.get_orbit_color(a, b, c);
            let [r,g,b] = color.to_rgb();
            // let [r,g,b] = [a,b,c];
            px[0] = (r * 255.0) as u8;
            px[1] = (g * 255.0) as u8;
            px[2] = (b * 255.0) as u8;
        }

        let raw = RawImage3d { 
            data: data.into(),
            width: TEXTURE_SIZE as u32,
            height: TEXTURE_SIZE as u32,
            depth: TEXTURE_SIZE as u32,
            format: ClientFormat::U8U8U8,
        };

        return Texture3d::new(display, raw).unwrap();
    }
}
