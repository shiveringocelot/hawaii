
shapes = {}

contextStack = {}
context = {}

loopVarId = 0

function createShape ()
	local name = "shape_" .. #shapes
	local shape = {
		contextType = "shape",
		name = name,

		stage = "de",
		deMode = "set",

		foldLines = {},
		deLines = {},
		orbitLines = {},

		vars = {},

		openLine = "float " .. name .. " (vec3 p, float scale) {\n\tfloat d = 0.0;",
		closeLine = "\treturn d / scale;\n}",

		-- probably infinity
		orbitInitLine = "vec4 dist = vec4(1e20);",
	}
	table.insert(shapes, shape)
	return shape
end

function createLoop (count)
	local var = "_i" .. loopVarId
	loopVarId = loopVarId + 1
	return {
		contextType = "loop",
		lines = {},
		openLine = string.format("for (int %s = 0; %s < %s; %s++) {", var, var, count, var),
		closeLine = "}",

		vars = context.vars,
	}
end

function evalVar (expr, type)
	if tonumber(expr) or string.match(expr, "^var%d+$") then
		return expr
	end
	local name = "var" .. #context.vars
	if not type then type = "float" end
	expr = string.format("%s %s = %s;", type, name, expr)
	table.insert(context.vars, expr) 
	return name
end

function evalExpr (base, ...)
	local args = {...}
	for i, val in ipairs(args) do
		args[i] = evalVar(val)
	end

	return string.format(base, table.unpack(args))
end

function enterContext (ctx)
	if context then
		table.insert(contextStack, context)
	end
	context = ctx
end

function exitContext ()
	local prev = context
	context = table.remove(contextStack)
	return prev
end

function writeFold (...)
	local line = evalExpr(...)
	if context.contextType == "loop" then
		table.insert(context.lines, line)
	else
		if context.stage == "de" then context.stage = "fold" end
		table.insert(context.foldLines, line)
	end
end

function writeDe (...)
	if context.stage == "fold" then
		error("can't add shapes in folding stage")
	end

	local value = evalExpr(...);

	-- Only use the deMode if the value is not a formula itself
	if not string.match(value, "^d%s*=") then
		if context.deMode == "set" then
			value = "d = " .. value .. ";"
		elseif context.deMode == "add" then
			value = "d = min(d, " .. value .. ");"
		elseif context.deMode == "sub" then
			value = "d = max(d, -" .. value .. ");"
		elseif context.deMode == "xor" then
			value = "d = max(d, " .. value .. ");"
		elseif context.deMode == "mix" then
			value = "d = lerp(d, " .. value .. ", " .. context.mixValue .. ");"
		end
	end

	table.insert(context.deLines, value)
end

function parseLine (line)
	local tk = {}
	for _tk in line:gmatch("[^%s]+") do table.insert(tk, _tk) end

	if #tk == 0 or tk[1]:sub(1, 1) == "#" then
		-- Ignore this line, it's either empty or a comment

	elseif tk[1] == "translate" then
		writeFold("p -= vec3(%s, %s, %s);", tk[2], tk[3], tk[4])

	elseif tk[1] == "scale" then
		if #tk == 2 then
			local s = tk[2]
			if tonumber(s) then
				s = 1.0 / s
			else
				s = evalVar("1.0/(" .. s .. ")")
			end
			writeFold("p *= %s; scale *= abs(%s);", s, s);
		else
			--writeFold("p *= vec3(1.0/%s, 1.0/%s, 1.0/%s); scale *= min();", tk[2], tk[3], tk[4])
		end

	elseif tk[1] == "rotate" then
		if #tk == 3 then
			local a = evalVar(tk[3])
			local mat = evalVar(string.format("mat2(cos(%s), sin(%s), -sin(%s), cos(%s))", a, a, a, a), "mat2")
			if tk[2] == "x" then
				writeFold("p.yz = %s * p.yz;", mat)
			elseif tk[2] == "y" then
				writeFold("p.xz = %s * p.xz;", mat)
			end
		end



	elseif tk[1] == "sphere" then
		writeDe("length(p) - %s", tk[2])

	elseif tk[1] == "box" then
		writeDe("boxShape(p, %s)", tk[2])

	elseif tk[1] == "tetrahedron" then
		writeDe("tetrahedronShape(p)")

	elseif tk[1] == "cone" then
		writeDe("coneShape(p, %s, %s)", tk[2], tk[3])


	elseif tk[1] == "add" then
		context.deMode = "add"
	elseif tk[1] == "subtract" then
		context.deMode = "sub"
	elseif tk[1] == "intersect" then
		context.deMode = "xor"

	elseif tk[1] == "mix" then
		context.deMode = "mix"
		context.mixValue = tk[2]

	elseif tk[1] == "round" then
		writeDe("d = d - %s;", tk[2])

	elseif tk[1] == "hollow" then
		writeDe("d = abs(d) - %s;", tk[2])



	elseif tk[1] == "infiniteRepeat" then
		writeFold("p = mod(p, %s);", tk[2])

	elseif tk[1] == "mirror" then
		if #tk == 4 then
			writeFold("mirrorFold(p, vec3(%s, %s, %s));", tk[2], tk[3], tk[4])
		elseif tk[2] == "x" then
			writeFold("p.x = abs(p.x);")
		elseif tk[2] == "y" then
			writeFold("p.y = abs(p.y);")
		elseif tk[2] == "z" then
			writeFold("p.z = abs(p.z);")
		elseif tk[2] == "xyz" then
			writeFold("p = abs(p);")
		end

	elseif tk[1] == "boxmirror" then
		writeFold("p = clamp(p, -%s, %s) * 2.0 - p;", tk[2], tk[2])

	elseif tk[1] == "spheremirror" then
		writeFold("scale *= sphereFold(p, %s, %s);", tk[2], tk[3])

	elseif tk[1] == "sierpinski" then
		writeFold("sierpinskiFold(p);")

	elseif tk[1] == "menger" then
		writeFold("mengerFold(p);")


	elseif tk[1] == "orbit" then
		writeFold("!O dist = min(dist, orbit(p, vec3(%s, %s, %s)));", tk[2], tk[3], tk[4])


	elseif tk[1] == "loop" then
		enterContext(createLoop(tk[2]))

	elseif tk[1] == "shape" then
		enterContext(createShape())

	elseif tk[1] == "end" then
		local inner = exitContext()
		if inner.contextType == "shape" then
			writeDe(inner.name .. "(p, scale)")
		elseif inner.contextType == "loop" then
			-- loops are only allowed for folds
			-- folds are compiled in oposite order as they are declared
			writeFold(inner.closeLine)
			for i, line in ipairs(inner.lines) do
				writeFold("\t" .. line)
			end
			writeFold(inner.openLine)
		end

	else
		error("Unknown line: " .. line)
	end
end

filename = arg[1] or "scenes/example.scene"

enterContext(createShape())

for line in io.lines(filename) do
	parseLine(line)
end

compiledCode = ""

function writeLine (line)
	compiledCode = compiledCode .. line .. "\n"
end

for j = #shapes, 1, -1 do
	local shape = shapes[j]

	if j < #shapes then writeLine("") end
	writeLine(shape.openLine)

	-- evaluations of complex expressions or parameter expressions
	for i = 1, #shape.vars do
		writeLine("\t" .. shape.vars[i])
	end

	-- write fold lines first from last to first
	for i = #shape.foldLines, 1, -1 do
		local line = shape.foldLines[i]
		if not string.match(line, "!O") then
			writeLine("\t" .. line)
		end
	end

	-- then write all the distance lines
	for i = 1, #shape.deLines do
		writeLine("\t" .. shape.deLines[i])
	end

	writeLine(shape.closeLine);
end

writeLine("\nfloat sdf (vec3 p) { return " .. context.name .. "(p, 1.0); }")

-- Write the color code
writeLine("\nvec4 surfaceColor (vec3 p) {\n\tfloat scale = 1.0;")
writeLine("\t" .. context.orbitInitLine)
for i = 1, #context.vars do
	writeLine("\t" .. context.vars[i])
end
-- orbit lines are mostly the fold lines plus orbit operations
for i = #context.foldLines, 1, -1 do
	local line = context.foldLines[i]
	if not string.match(line, "!O") then
		writeLine("\t" .. line)
	else
		writeLine("\t" .. string.gsub(line, "!O%s*", ""))
	end
end
writeLine("\treturn dist;\n}")

if os.getenv("TEST") then
	print(compiledCode)
else
	shaderFile = io.open("shaders/fragment.glsl")
	shaderText = shaderFile:read("*all"):gsub("//! COMPILED CODE", compiledCode)
	print(shaderText)
end